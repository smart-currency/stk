/**
 * Copyright 2018 Smartokenizer
 *
 * @author: Christian Chiama
 * @author: Paperini Alessandro
 * 
 * @version: 2.0.0
 * @since:2018
 * @see:http://smartokenizer.org
 * 
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import * as _ from 'lodash';

export abstract class LangUtilities {

	/**
		* Flatten object, if the object is not plain object returns directly the passed value
		* @param value
		* @returns {any}
		*/
	static flattenObject(value: any): any {
		if (!_.isPlainObject(value)) {
			return value;
		}

		let ret = {};
		this.doFlattenObject(value, '', ret);
		return ret;
	}

	/**
		* Unflatten object, if the object is not plain object returns directly the passed value
		* @param {{[p: string]: any}} map
		* @returns {any}
		*/
	static unflattenObject(map: { [key: string]: any }): any {

		if (!_.isPlainObject(map)) {
			return map;
		}
		
		let keys = Object.keys(map).sort();
		let ret = {};

		for (let key of keys) {
			let cur = ret;
			let parts = key.split('.');
			for (let i = 0; i < parts.length - 1; i++) {
				cur[parts[i]] = cur[parts[i]] || {};
				cur = cur[parts[i]];
			}
			cur[parts[parts.length - 1]] = map[key];
		}

		return ret;

	}

	private static doFlattenObject(value: any, prefix: string, ret: { [key: string]: any }) {
		if (!_.isPlainObject(value)) {
			ret[prefix] = value;
			return;
		}

		let keys = Object.keys(value);
		keys.forEach(key => {
			let newKey = (prefix ? prefix + '.' : '') + key;
			this.doFlattenObject(value[key], newKey, ret);
		});

		return ret;
	}

}
