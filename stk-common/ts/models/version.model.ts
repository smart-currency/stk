/**
 * Copyright 2018 Smartokenizer
 *
 * @author: Christian Chiama
 * @author: Paperini Alessandro
 * 
 * @version: 2.0.0
 * @since:2018
 * @see:http://smartokenizer.org
 * 
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { BaseEntity } from "./base.model";

export class VersionModel extends BaseEntity<VersionModel> {
    apiVersion:string;
    appVersion:string;
    raw: string;
    major: number;
    minor: number;
    patch: number;
    prerelease: string[];
    build: string;
    version: string;
    codeName: string;
    isSnapshot: boolean;
    full: string;
    branch: string;
    commitSHA: string;
}

