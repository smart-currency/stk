/**
 * Copyright 2018 Smartokenizer
 *
 * @author: Christian Chiama
 * @author: Paperini Alessandro
 * 
 * @version: 2.0.0
 * @since:2018
 * @see:http://smartokenizer.org
 * 
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ActivatedRoute, ActivatedRouteSnapshot, Data } from '@angular/router';

export interface IRoutingHandle {
    navigateToUrl(url: string, data: string);
}

export class RoutingHandler<K> implements IRoutingHandle {
    navigateToUrl(url: string, data: string) {
        throw new Error("Method not implemented.");
    }
    public static navigateToUrl(url: string, data: string) {
        window.location.href = url;
        localStorage.setItem('data', data)
    }

    static getRouteData(currentRoute: ActivatedRoute): Data {
        const route: ActivatedRoute = currentRoute;

        if (!currentRoute.parent) {
            return Object.assign({}, currentRoute.snapshot.data);
        }

        const data: Data = RoutingHandler.getRouteData(currentRoute.parent);
        for (const key of Object.keys(currentRoute.snapshot.data)) {
            data[key] = currentRoute.snapshot.data[key];
        }

        return data;
    }

    static getRouteParams(currentRoute: ActivatedRouteSnapshot): Data {
        const route: ActivatedRouteSnapshot = currentRoute;

        if (!currentRoute.parent) {
            return Object.assign({}, currentRoute.params);
        }

        const data: Data = RoutingHandler.getRouteParams(currentRoute.parent);
        for (const key of Object.keys(currentRoute.params)) {
            data[key] = currentRoute.params[key];
        }

        return data;
    }
}
