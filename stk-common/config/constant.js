/*
* ,---.                               ,--------.         ,--.                      ,--.                         
* '   .-'  ,--,--,--.  ,--,--. ,--.--. '--.  .--'  ,---.  |  |,-.   ,---.  ,--,--,  `--' ,-----.  ,---.  ,--.--. 
* `.  `-.  |        | ' ,-.  | |  .--'    |  |    | .-. | |     /  | .-. : |      \ ,--. `-.  /  | .-. : |  .--' 
* .-'    | |  |  |  | \ '-'  | |  |       |  |    ' '-' ' |  \  \  \   --. |  ||  | |  |  /  `-. \   --. |  |    
* `-----'  `--`--`--'  `--`--' `--'       `--'     `---'  `--'`--'  `----' `--''--' `--' `-----'  `----' `--'    
*   
*/
const STK_DOMAIN = 'http://smartokenizer.org'
const STK_CONSOLE_DOMAIN_BASE_URL = 'http://console.smartokenizer.org/'
const BASE_URL = '/api/v1/'
const ENDPOINT = {
  AUTH: 'oauth/token',
  VERSION: 'app/version'
}

const SERVICE_WORKER = {
  CONFIG: './config/json/ngsw-worker.js'
}
const I18N = {
  CONFIG: '../assets/i18n/'
}
const WS_URL = 'ws://echo.websocket.org/'
const AVATAR_URL = '../assets/default-avatar.png'
const COUNTRY_CONFIG = {
  URL: '../../assets/json/countries.json'
}

const TEST_AVATAR_URL = 'https://lh3.googleusercontent.com/-1i_YGB6ZCx8/AAAAAAAAAAI/AAAAAAAAAAA/ACLGyWBn3cXFo0QeNhr_Os_VdeITAH2SqA/s64-c-mo/photo.jpg'

const firebaseAuth = {
  production: false,
  config: {
    apiKey: 'AIzaSyDGs47bD1PsPQ95r4q_k2v97isdmaYlwwQ',
    authDomain: 'smartokenizer.firebaseapp.com',
    databaseURL: 'https://smartokenizer.firebaseio.com',
    projectId: 'smartokenizer',
    storageBucket: '',
    messagingSenderId: '255806824107'
  }
}

export {
  BASE_URL,
  ENDPOINT,
  SERVICE_WORKER,
  WS_URL,
  COUNTRY_CONFIG,
  AVATAR_URL,
  I18N,
  TEST_AVATAR_URL,
  firebaseAuth
}
