import { getDeepKeys } from '../app/utils/function';

describe('Translations in app', () => {

	// all the languages should have the same keys
	it('should be coerent', () => {

		const it = require('../assets/i18n/it.json');
		const en = require('../assets/i18n/en.json');

		const itKeys = getDeepKeys(it).sort();
		const enKeys = getDeepKeys(en).sort();

		expect(itKeys.length).toBeGreaterThan(0);
		expect(itKeys).toEqual(enKeys);

	});

});
