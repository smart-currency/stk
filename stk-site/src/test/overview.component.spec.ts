import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MaterialModule } from '@app/shared/material/material.module';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { VersionService } from '@app/service/version.service';
import { OverviewComponent } from '@app/components/overview/overview.component';
import { HeaderComponent } from '@app/components/header/header.component';
import { CardComponent } from '@app/components/card/card.component';
import { FooterComponent } from '@app/components/footer/footer.component';

describe('Stk Site OverviewComponent', () => {
  let component: OverviewComponent;
  let fixture: ComponentFixture<OverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [VersionService],
      imports: [MaterialModule, HttpClientModule],
      declarations: [
        OverviewComponent, 
        HeaderComponent, 
        CardComponent, 
        FooterComponent
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
