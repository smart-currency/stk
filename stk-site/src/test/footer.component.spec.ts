import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VersionService } from '@app/service/version.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { FooterComponent } from '@app/components/footer/footer.component';

describe('Stk Site FooterComponent', () => {
  let component: FooterComponent;
  let fixture: ComponentFixture<FooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers:[HttpClient, VersionService],
      imports:[HttpClientModule],
      declarations: [ FooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
