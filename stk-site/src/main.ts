import { enableProdMode, ApplicationRef } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { enableDebugTools } from '@angular/platform-browser';
import { StkWebSiteModule } from '@app/stk-site.module';
import { environment } from '@env/environment-prod';

if (environment.production) {
  enableProdMode();
}
  platformBrowserDynamic().bootstrapModule(StkWebSiteModule).then((module) => {
    let applicationRef = module.injector.get(ApplicationRef);
    let appSiteComponent = applicationRef.components[0];
    enableDebugTools(appSiteComponent);
  }).catch(err => console.log(err));
