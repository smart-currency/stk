
import { NavItem, ProjectItem } from '@stk/common/src';

export const TopMenu: Array<NavItem> = [];
export const Projects: Array<ProjectItem> = [];


export const navMenu = {
  item: [
    {
      label: 'Features',
      link: 'features',
      icon: ''
    },
    {
      label: 'Docs',
      link: 'docs',
      icon: ''
    },
    {
      label: 'Version',
      link: 'version',
      icon: ''
    },
    {
      label: 'Contact',
      link: 'contact',
      icon: ''
    }
  ]
}.item.map((i) => TopMenu.push(i));

export const project = {
  item: [
    {
      image: 'https://c.staticblitz.com/assets/stack_icons/svelte-3aa988b3fd849eb5f46b340435d667ce84d0e5e6855cc20a10f1816385ecdac8.png',
      title: 'Svelte',
      subTitle: 'svelte',
      link: '.',
      icon: 'arrow_forward'
    },
    {
      image: 'https://c.staticblitz.com/assets/stack_icons/auth0-ccbd5d41a7b9e9fd6734ba6a102eda8fd7ef04038f85e5b475df9a0053be1aba.png',
      title: 'Auth0',
      subTitle: 'auth0',
      link: '.',
      icon: 'arrow_forward'
    },
    {
      image: 'https://c.staticblitz.com/assets/stack_icons/angularjs-a2c9b97c1b18bea5a7ca77c9b3019cc8594788fd477fb93ec7ba7f8a85974e03.png',
      title: 'Angular 4/5',
      subTitle: 'angular',
      link: '.',
      icon: 'arrow_forward'
    },
    {
      image: 'https://c.staticblitz.com/assets/stack_icons/js-8a69bb6a0a013b79ae96f4000df77ed27b1ceee3b850763f434c8e29462969a6.png',
      title: 'Javascript',
      subTitle: 'javascript',
      link: '.',
      icon: 'arrow_forward'
    },
    {
      image: 'https://c.staticblitz.com/assets/stack_icons/typescript-a327a87a41c763abce131184edbc6b4b9adfb14d74909defc365511359ac435e.png',
      title: 'Typescript',
      subTitle: 'typescript',
      link: '.',
      icon: 'arrow_forward'
    }
  ]
}.item.map((i) => Projects.push(i));
