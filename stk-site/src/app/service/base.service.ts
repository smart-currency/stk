import { HttpHeaders, HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { Service } from "@stk/common/src/service";
import { RestResponse } from "@stk/common/src/models";
import { BASE_URL } from '@stk/common/config/constant';


const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

export abstract class BaseService<K> implements Service<RestResponse<K>>{

    http: HttpClient;
    baseUrl: string = BASE_URL;

    /**
    * Creates a new BaseService with the injected Http.
    * @param {Http} http - The injected Http.
    * @constructor
    */
    constructor(http: HttpClient) {
        this.http = http
    }

    httpPost(endpoint: string): Observable<RestResponse<K>> {
        return this.http.post<RestResponse<K>>(this.baseUrl.concat(endpoint), httpOptions)
    }
    httpGet(endpoint: string): Observable<RestResponse<K>> {
        return this.http.get<RestResponse<K>>(this.baseUrl.concat(endpoint))
    }
    httpPut(endpoint: string): Observable<RestResponse<K>> {
        return this.http.post<RestResponse<K>>(this.baseUrl.concat(endpoint), httpOptions)
    }
    httpPatch(endpoint: string): Observable<RestResponse<K>> {
        return this.http.patch<RestResponse<K>>(this.baseUrl.concat(endpoint), httpOptions)
    }
    httpOptions(endpoint: string): Observable<RestResponse<K>> {
        return this.http.options<RestResponse<K>>(this.baseUrl.concat(endpoint), httpOptions)
    }
    httpDelete(endpoint: string): Observable<RestResponse<K>> {
        return this.http.post<RestResponse<K>>(this.baseUrl.concat(endpoint), httpOptions)
    }
    httpCors(endpoint: string, callback: string): Observable<RestResponse<K>> {
        return this.http.jsonp<RestResponse<K>>(this.baseUrl.concat(endpoint), callback)
    }
    httpRequest(method: string, endpoint: string): Observable<RestResponse<K>> {
        return this.http.request<RestResponse<K>>(method, this.baseUrl.concat(endpoint), httpOptions)
    }
}
