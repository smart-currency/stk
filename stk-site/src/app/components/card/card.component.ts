import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'stk-app-site-card',
  templateUrl: './card.component.html',
  encapsulation:ViewEncapsulation.None
})
export class CardComponent implements OnInit {

  @Input() image:string;
  @Input() title:string;
  @Input() description:string;
  @Input() withImage:boolean = false;


  constructor() { }

  ngOnInit() {
  }
}
