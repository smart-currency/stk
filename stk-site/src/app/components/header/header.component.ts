import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import { PKG } from 'config';

@Component({
  selector: 'stk-app-site-header',
  templateUrl: './header.component.html',
  encapsulation:ViewEncapsulation.None
})
export class HeaderComponent implements OnInit {

   tagline:string
   subTagline:string

  constructor() {
    this.tagline = (<any>PKG).description;
    this.subTagline = (<any>PKG).tagline
   }

  ngOnInit() {
  }

}
