import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, Inject, OnDestroy } from '@angular/core';
import { FirebaseAuthService } from '@app/shared/firebase/firebase.auth.service';
import { Observable } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { StkFirebaseUser } from '@stk/common/src/models/base.model';
import { RoutingHandler } from '@app/utils/routing.handler';


@Component({
  selector: 'stk-app-site-signin',
  templateUrl: './signin.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  providers: [FirebaseAuthService]
})
export class SigninComponent implements OnInit {

  messages = [];
  connection;
  message;
 
  constructor(private authService: FirebaseAuthService,
    public dialogRef: MatDialogRef<SigninComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }


  ngOnInit() {}


  closeDialog() {
    this.dialogRef.close();
  }

  signinWithGoogle() {
    Observable.fromPromise(this.authService.signInWithGoogle())
      .take(1)
      .subscribe((user:StkFirebaseUser) => {
        this.closeDialog()
        RoutingHandler.navigateToUrl('http://console.smartokenizer.org/index.html', '');
        console.log(user)
      })
  }
  signinWithFacebook() {
    Observable.fromPromise(this.authService.signInWithFacebook())
      .take(1)
      .subscribe((user:StkFirebaseUser) => {
        this.closeDialog()
        console.log(user)
      })
  }
  signinWithTwitter() {
    Observable.fromPromise(this.authService.signInWithTwitter())
      .take(1)
      .subscribe((user:StkFirebaseUser) => {
        this.closeDialog()
        console.log(user)
      })
  }
  signinWithGithub() {
    Observable.fromPromise(this.authService.signInWithGithub())
      .take(1)
      .subscribe((user:StkFirebaseUser) => {
        this.closeDialog()
        console.log(user)
      })
  }

}
