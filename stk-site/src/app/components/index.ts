import { NgModule, SkipSelf, Optional } from '@angular/core';
import { SharedModule } from '@app/shared/shared.module';
import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from '@app/components/navbar/navbar.component';
import { HeaderComponent } from '@app/components/header/header.component';
import { OverviewComponent } from '@app/components/overview/overview.component';
import { SettingsComponent } from '@app/components/settings/settings.component';
import { CardComponent } from '@app/components/card/card.component';
import { ReleaseComponent } from '@app/components/release/release.component';
import { ProjectComponent } from '@app/components/project/project.component';
import { SigninComponent } from '@app/components/signin/signin.component';


const sharedComponents = [
    NavbarComponent,
    HeaderComponent,
    SigninComponent,
    OverviewComponent,
    SettingsComponent,
    ProjectComponent,
    CardComponent,
    ReleaseComponent,
    FooterComponent
]

@NgModule({
    imports: [
        SharedModule
    ],
    exports: [sharedComponents],
    declarations: [sharedComponents],
    entryComponents: [SettingsComponent,SigninComponent],
    providers: []
})
export class SharedComponentsModule {
    constructor(@SkipSelf() @Optional() public sharedComponentsModule: SharedComponentsModule) { }
}
