import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'stk-app-site-overview',
  templateUrl: './overview.component.html',
  encapsulation: ViewEncapsulation.None
})
export class OverviewComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
