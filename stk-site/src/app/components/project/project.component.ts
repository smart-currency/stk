import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Projects } from 'config';
import { ProjectItem } from '@stk/common/config/constant';

@Component({
  selector: 'stk-app-site-project',
  templateUrl: './project.component.html',
  encapsulation: ViewEncapsulation.None
})
export class ProjectComponent implements OnInit {


  projectItems:Array<ProjectItem> = [];

  constructor() { }

  ngOnInit() {this.projectItems = Projects;}

}
