import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'stk-app-release',
  templateUrl: './release.component.html',
  encapsulation:ViewEncapsulation.None
})
export class ReleaseComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
