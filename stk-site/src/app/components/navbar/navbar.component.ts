import { Component, OnInit, ViewEncapsulation, Input, ChangeDetectionStrategy, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SettingsComponent } from '@app/components/settings/settings.component';
import { SigninComponent } from '@app/components/signin/signin.component';
import { PKG, TopMenu } from 'config';
import { NavItem } from '@stk/common/src';


@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'stk-app-site-navbar',
  templateUrl: './navbar.component.html',
  encapsulation: ViewEncapsulation.None
})
export class NavbarComponent implements OnInit {

  title: string = (<any>PKG).title

  navIcon: Array<String> = []
  navBarMenu: Array<NavItem> = [];


  constructor(public dialog: MatDialog) { }

  ngOnInit() {
    this.createMenu()
  }

  createMenu() {
    this.navBarMenu = TopMenu;
    this.navIcon.push('notifications')
    this.navIcon.push('settings')
    this.navIcon.push('color_lens')
  }

  openSettingsDialog() {
    const dialogRef = this.dialog.open(SettingsComponent);
    dialogRef.afterClosed().subscribe(result => {
      console.log(result)
    })
  }
  openSigninDialog() {
    const dialogRef = this.dialog.open(SigninComponent, {
      id: 'signinCmp'
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(result)
    })
  }
}
