import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { VersionService } from '@app/service/version.service';
import { ENDPOINT } from '@stk/common/config/constant';

@Component({
  selector: 'stk-app-site-footer',
  templateUrl: './footer.component.html',
  providers:[VersionService],
  encapsulation:ViewEncapsulation.None
})
export class FooterComponent implements OnInit {

  version:string
  constructor(private versionService:VersionService) {}

  ngOnInit() {
    this.versionService.httpGet(ENDPOINT.VERSION).subscribe((v) => {
      this.version = v.data.appVersion
    })
  }

}
