import { Component, OnInit, ViewEncapsulation, Inject, Input } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  moduleId: module.id,
  selector: 'stk-app-site-settings',
  templateUrl: './settings.component.html',
  providers:[],
  encapsulation: ViewEncapsulation.None
})
export class SettingsComponent {


  formGroup: FormGroup;
  isNonLinear = false;
  isNonEditable = false;

  nameFormGroup: FormGroup;
  emailFormGroup: FormGroup;


  steps = [
    {label: 'Confirm your name', content: 'Last name, First name.'},
    {label: 'Confirm your contact information', content: '123-456-7890'},
    {label: 'Confirm your address', content: '1600 Amphitheater Pkwy MTV'},
    {label: 'You are now done', content: 'Finished!'}
  ];

  get formArray(): AbstractControl | null { return this.formGroup.get('formArray'); }

  constructor(private _formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<SettingsComponent>,
     @Inject(MAT_DIALOG_DATA) public data: any) { 
     }

     ngOnInit() {
      this.formGroup = this._formBuilder.group({
        formArray: this._formBuilder.array([
          this._formBuilder.group({
            firstNameFormCtrl: ['', Validators.required],
            lastNameFormCtrl: ['', Validators.required],
          }),
          this._formBuilder.group({
            emailFormCtrl: ['', Validators.email]
          }),
        ])
      });
  
      this.nameFormGroup = this._formBuilder.group({
        firstNameCtrl: ['', Validators.required],
        lastNameCtrl: ['', Validators.required],
      });
  
      this.emailFormGroup = this._formBuilder.group({
        emailCtrl: ['', Validators.email]
      });
    }

    
  onNoClick(): void {
    this.dialogRef.close();
  }

}
