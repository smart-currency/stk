import { NgModule,Optional, SkipSelf } from '@angular/core';

import {
  MatButtonModule,
  MatCheckboxModule,
  MatToolbarModule,
  MatOptionModule,
  MatProgressBarModule,
  MatDialogModule,
  MatSnackBarModule,
  MatSelectModule,
  MatInputModule,
  MatSidenavModule,
  MatStepperModule,
  MatHorizontalStepper,
  MatCardModule,
  MatIconModule,
  MatRadioModule,
  MatProgressSpinnerModule,
  MatTooltipModule,
} from '@angular/material';

import { PlatformModule } from '@angular/cdk/platform';
import { ObserversModule } from '@angular/cdk/observers';

const materialModuleImport = [
  MatButtonModule,
  MatCheckboxModule,
  MatToolbarModule,
  MatOptionModule,
  MatProgressBarModule,
  MatDialogModule,
  MatSnackBarModule,
  MatSelectModule,
  MatInputModule,
  MatSidenavModule,
  MatStepperModule,
  MatCardModule,
  MatIconModule,
  MatRadioModule,
  MatProgressSpinnerModule,
  MatTooltipModule,
]

@NgModule({
  imports: [
    PlatformModule,
    ObserversModule,
    materialModuleImport
  ],
  exports: [
    PlatformModule,
    ObserversModule,
    materialModuleImport
  ],
})
export class MaterialModule { 
  constructor(@SkipSelf() @Optional() public material:MaterialModule){
  }
}
