import { TranslateModule} from '@ngx-translate/core';
import { TranslateMessageFormatCompiler } from 'ngx-translate-messageformat-compiler';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NgModule, SkipSelf, Optional } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { FirebaseModule } from './firebase/firebase.module';
import { MaterialModule } from '@app/shared/material/material.module';
import { TranslateLoader } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateCompiler } from '@ngx-translate/core';

export function HttpLoaderFactory(http: HttpClient) {
	return new TranslateHttpLoader(http);
}

@NgModule({
  imports: [
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: HttpLoaderFactory,
				deps: [HttpClient]
			},
			compiler: {
				provide: TranslateCompiler,
				useClass: TranslateMessageFormatCompiler
			}
		})
],
  declarations: [],
  exports: [  
		CommonModule,
		TranslateModule,
		HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    FirebaseModule,
		MaterialModule
  ]
})
export class SharedModule {
  constructor(@SkipSelf() @Optional() public sharedModule:SharedModule){}

 }
