import { ServiceWorkerModule } from '@angular/service-worker';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SharedModule } from '@app/shared/shared.module';
import { SERVICE_WORKER } from '@stk/common/config/constant';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StkWebSiteComponent } from '@app/stk-site.component';
import { SharedComponentsModule } from '@app/components';
import { environment } from '@env/environment-prod';

@NgModule({
  declarations: [
    StkWebSiteComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    SharedComponentsModule,
    ServiceWorkerModule.register(SERVICE_WORKER.CONFIG, { enabled: environment.production })
  ],
  providers: [],
  bootstrap: [StkWebSiteComponent]
})
export class StkWebSiteModule { }
