import { ElementRef } from "@angular/core";
import { OverlayContainer } from "@angular/cdk/overlay";
import _ from 'lodash';


export function getDeepKeys(obj) {
  if (!(obj instanceof Object && Object.keys(obj).length > 0)) {
      return [];
  } else {
    const firstKey = Object.keys(obj)[0];

    return [firstKey]
      .concat(getDeepKeys(_.omit(obj, firstKey)))
      .concat(getDeepKeys(obj[firstKey]));
  }
} 

export class StkSiteUtils {

  dark = false;

  constructor(
    private _element: ElementRef,
    private _overlayContainer: OverlayContainer) { }

  toggleFullscreen() {
    let elem = this._element.nativeElement.querySelector('.stk');
    if (elem.requestFullscreen) {
      elem.requestFullscreen();
    } else if (elem.webkitRequestFullScreen) {
      elem.webkitRequestFullScreen();
    } else if (elem.mozRequestFullScreen) {
      elem.mozRequestFullScreen();
    } else if (elem.msRequestFullScreen) {
      elem.msRequestFullScreen();
    }
  }

  toggleTheme() {
    const darkThemeClass = 'dark-theme';

    this.dark = !this.dark;

    if (this.dark) {
      this._element.nativeElement.classList.add(darkThemeClass);
      this._overlayContainer.getContainerElement().classList.add(darkThemeClass);
    } else {
      this._element.nativeElement.classList.remove(darkThemeClass);
      this._overlayContainer.getContainerElement().classList.remove(darkThemeClass);
    }
  } 
}


