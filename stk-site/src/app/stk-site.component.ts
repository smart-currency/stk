import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'smartokenizer-app-site-root',
  templateUrl: './stk-site.component.html',
  providers:[],
  encapsulation: ViewEncapsulation.None
})
export class StkWebSiteComponent {
  title = 'stk' ;
}


