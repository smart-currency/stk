import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { SidenavService } from '@app/module/sidenav/service/sidenav.service';
import * as PKG from '../../../../package.json';

@Component({
  selector: 'stk-app-navbar',
  templateUrl: './navbar.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: []
})
export class NavbarComponent implements OnInit {

  @ViewChild('fullSidenav')
  public fullSidenav: MatSidenav;

  title: string = (<any>PKG).title
  constructor(private sidenav: SidenavService) {
  }

  ngOnInit() {
  }

  toggle() {
    this.sidenav.toggle();
  }

  toggleRight() {
    this.fullSidenav.toggle();
  }

}
