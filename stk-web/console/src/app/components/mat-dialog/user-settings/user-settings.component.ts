import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'stk-app-user-settings-dialog',
  templateUrl: './user-settings.component.html',
  encapsulation: ViewEncapsulation.None
})
export class UserSettingsComponent {

  constructor(
    public dialogRef: MatDialogRef<UserSettingsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }
}
