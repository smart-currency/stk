import { Component, OnInit, ViewEncapsulation, Inject, Input } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'stk-app-version-dialog',
  templateUrl: './version.component.html',
  encapsulation: ViewEncapsulation.None
})
export class StkVersionComponent {

  @Input() title: string;
  @Input() version: string;
  @Input() build: string;
  @Input() release: string;
  @Input() mayor: string;
  @Input() mynor: string;

  constructor(public dialogRef: MatDialogRef<StkVersionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
