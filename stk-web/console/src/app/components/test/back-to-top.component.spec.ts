import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '@app/shared/material/material.module';
import { StkBackToTopComponent } from '@app/components/mat-button/back-to-top.component';

describe('StkBackToTopComponent', () => {
	let component: StkBackToTopComponent;
	let fixture: ComponentFixture<StkBackToTopComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				MaterialModule,
				TranslateModule.forRoot()
			],
			declarations: [ StkBackToTopComponent ]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(StkBackToTopComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
