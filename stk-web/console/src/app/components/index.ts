import { NgModule, SkipSelf, Optional } from '@angular/core';
import { NavbarComponent } from '@app/components/mat-navbar/navbar.component';
import { SharedModule } from '@app/shared/shared.module';
import { CountrySelectComponent } from '@app/components/mat-select/country/country-select.component';
import { RouterModule } from '@angular/router';
import { StkSearchModule } from '@app/module/search/search.module';
import { StkVersionComponent } from '@app/components/mat-dialog/version/version.component';
import { SelectComponent } from '@app/components/mat-select/base-select/select.component';
import { MatMenuComponent } from '@app/components/mat-menu/mat-menu.component';
import { AvatarComponent } from '@app/module/user/avatar/avatar.component';

const sharedComponents = [
   AvatarComponent,
   NavbarComponent,
   StkVersionComponent,
   MatMenuComponent,
   CountrySelectComponent,
   SelectComponent,
]

@NgModule({
    declarations: [sharedComponents],
    imports: [
        RouterModule,
        SharedModule,
        StkSearchModule
    ],
    exports: [sharedComponents],
    entryComponents: [],
    providers: []
})
export class StkComponentsModule {
    constructor(@SkipSelf() @Optional() public sharedComponentsModule: StkComponentsModule) { }

}
