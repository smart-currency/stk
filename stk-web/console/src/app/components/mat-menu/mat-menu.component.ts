import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'stk-app-mat-menu',
  templateUrl: './mat-menu.component.html',
  styleUrls: ['./mat-menu.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MatMenuComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
