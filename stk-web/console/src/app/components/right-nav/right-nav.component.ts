import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'stk-app-right-nav',
  templateUrl: './right-nav.component.html',
  encapsulation: ViewEncapsulation.None
})
export class RightNavComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
