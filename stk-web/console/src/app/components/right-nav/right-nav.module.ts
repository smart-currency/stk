import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RightNavComponent } from './right-nav.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [RightNavComponent]
})
export class StkRightNavModule { }
