import { Component, OnInit, ViewEncapsulation, Input, HostBinding } from '@angular/core';
import { ICountry } from '@stk/common/src/models';
import * as _ from 'lodash';
import { CountryService } from '@app/api/country.service';

@Component({
  selector: 'stk-app-country-select',
  template: `<mat-form-field class="w-100 pt-32">
              <mat-select placeholder="State">
                <mat-option *ngFor="let c of countries; let i = index;" [value]="getValue(c)">{{getName(c)}}</mat-option>
              </mat-select>
            </mat-form-field>`,
  providers:[CountryService],
  encapsulation: ViewEncapsulation.None
})
export class CountrySelectComponent {

  @Input() flag: boolean = false;
  @Input() setValue: string = 'cca3';
  @Input() setName: string = 'name.common';

  public countries: ICountry[];
  public baseUrl: string = '../../../assets/json/data/';

  constructor(private countryPickerService: CountryService) {
    this.countryPickerService.getCountries().subscribe(countries => {
      this.countries = countries.sort((a: ICountry, b: ICountry) => {
        let na = this.getName(a);
        let nb = this.getName(b); 
        if (na > nb) {
          return 1;
        }
        if (na < nb) {
          return -1;
        }
        return 0;
      });
    });
    this.baseUrl = countryPickerService.baseUrl;
  }

  public getValue(obj: ICountry) {
    return _.get(obj, this.setValue);
  }

  public getName(obj: ICountry) {
    return _.get(obj, this.setName);
  }

}
