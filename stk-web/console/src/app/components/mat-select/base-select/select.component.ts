import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'stk-app-select',
  templateUrl: './select.component.html',
  encapsulation: ViewEncapsulation.None
})
export class SelectComponent implements OnInit {

  elements = [
    {value: 'steak-0', viewValue: 'Visualizza tutti i progetti'},
    {value: 'pizza-1', viewValue: 'Aggiungi progetto'},
    {value: 'tacos-2', viewValue: 'Recenti'},
    {value: 'tacos-2', viewValue: 'Smartokenizer'},
    {value: 'tacos-2', viewValue: 'Vip2Zip'}

  ];

  constructor() { }
  ngOnInit() {
  }

}
