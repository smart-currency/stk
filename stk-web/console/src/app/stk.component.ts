import { Component, ViewEncapsulation, OnInit } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'smartokenizer-app-root',
  templateUrl: './stk.component.html',
  encapsulation:ViewEncapsulation.None
})
export class SmartokenizerAppComponent implements OnInit {

  title = 'stk';

  constructor() { }

	ngOnInit() {
	
	}
}
