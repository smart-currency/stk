import { NgModule, SkipSelf, Optional, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { TranslateLoader, TranslateModule, TranslateCompiler } from '@ngx-translate/core';
import { TranslateMessageFormatCompiler } from 'ngx-translate-messageformat-compiler';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FirebaseModule } from './firebase/firebase.module';
import { MaterialModule } from './material/material.module';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { PreloaderModuleModule } from './preloader/preloader.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NotificationService } from '@app/shared/service/notification.service';
import { TranslateServiceConfiguration } from '@app/shared/service/translate.service';
import { I18N } from '@stk/common/config/constant';

export function HttpLoaderFactory(http: HttpClient) {
	return new TranslateHttpLoader(http);
}

@NgModule({
	imports: [
		BrowserAnimationsModule,
		PreloaderModuleModule,
		MaterialModule,
		InfiniteScrollModule,
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: HttpLoaderFactory,
				deps: [HttpClient]
			},
			compiler: {
				provide: TranslateCompiler,
				useClass: TranslateMessageFormatCompiler
			}
		})],
	declarations: [],
	exports: [
		CommonModule,
		TranslateModule,
		HttpClientModule,
		FormsModule,
		ReactiveFormsModule,
		PreloaderModuleModule,
		FirebaseModule,
		MaterialModule,
	],
})
export class SharedModule {

	public static forRoot(): ModuleWithProviders {
		return {
			ngModule: SharedModule,
			providers: [
				NotificationService,
				TranslateServiceConfiguration]
		};
	}

}
