import { NgModule,Optional, SkipSelf } from '@angular/core';

import {
  MatButtonModule,
  MatCheckboxModule,
  MatToolbarModule,
  MatOptionModule,
  MatProgressBarModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatMenuModule,
  MatDialogModule,
  MatSnackBarModule,
  MatSelectModule,
  MatInputModule,
  MatSidenavModule,
  MatStepperModule,
  MatHorizontalStepper,
  MatCardModule,
  MatIconModule,
  MatRadioModule,
  MatProgressSpinnerModule,
  MatTabsModule,
  MatTooltipModule,
  MatDividerModule,
  MatIconRegistry,
} from '@angular/material';

import { PlatformModule } from '@angular/cdk/platform';
import { ObserversModule } from '@angular/cdk/observers';
import { DomSanitizer } from '@angular/platform-browser';
import { icons } from 'config/icons';


const materialModuleImport = [
  MatButtonModule,
  MatCheckboxModule,
  MatToolbarModule,
  MatOptionModule,
  MatDividerModule,
  MatProgressBarModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatMenuModule,
  MatDialogModule,
  MatSnackBarModule,
  MatSelectModule,
  MatInputModule,
  MatSidenavModule,
  MatStepperModule,
  MatCardModule,
  MatIconModule,
  MatRadioModule,
  MatProgressSpinnerModule,
  MatTabsModule,
  MatTooltipModule,
]

@NgModule({
  imports: [
    PlatformModule,
    ObserversModule,
    materialModuleImport
  ],
  exports: [
    PlatformModule,
    ObserversModule,
    materialModuleImport
  ],
  providers:[]

})
export class MaterialModule { 

  importIcons:Array<string> = icons;

  constructor(@SkipSelf() @Optional() public material:MaterialModule,
  iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) { 

    this.importIcons.forEach(iconName => {
      iconRegistry.addSvgIcon(
        iconName, sanitizer.bypassSecurityTrustResourceUrl('../../../assets/svg/' + iconName + '.svg')
      );
    }); 

   
  }
}
