import { NgModule } from '@angular/core';
import { LoadingModule,ANIMATION_TYPES } from 'ngx-loading';
import { PreloaderComponent } from './preloader.component';


@NgModule({
    declarations: [PreloaderComponent],
    imports: [
        LoadingModule.forRoot({
            animationType: ANIMATION_TYPES.cubeGrid,
            backdropBackgroundColour: 'rgba(0,0,0,0.1)',
            backdropBorderRadius: '4px',
            primaryColour: '#ffffff',
            secondaryColour: '#ffffff',
            tertiaryColour: '#ffffff'
        })
    ],
    exports:[PreloaderComponent],
    providers: [],
    bootstrap: []
})
export class PreloaderModuleModule { }
