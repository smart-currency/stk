import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'stk-app-preloader',
  template: `<ngx-loading [show]="loading" [config]="{ backdropBorderRadius: '14px' }"></ngx-loading>`,
  encapsulation:ViewEncapsulation.None
})
export class PreloaderComponent implements OnInit {

 loading: boolean = false;

  constructor() { }

  ngOnInit() {
    this.loading = true
  }

  

}
