import { NgModule, SkipSelf, Optional } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { FirebaseAuthService } from './firebase.auth.service';
import { firebaseAuth } from '@stk/common/config/constant';


@NgModule({
  imports: [
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(firebaseAuth.config,firebaseAuth.config.projectId)
  ],
  providers:[FirebaseAuthService]
})
export class FirebaseModule { 
  constructor(@SkipSelf() @Optional() public firebaseModule:FirebaseModule){}
}
