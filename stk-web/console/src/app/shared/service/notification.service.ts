import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class NotificationService {

	constructor(private snackbar: MatSnackBar) { }

	info(detail: string, summary?: string, options?: any): void {
		this.notify('info', detail, summary, options);
	}

	debug(detail: string, summary?: string, options?: any): void {
		this.notify('debug', detail, summary, options);
	}

	warn(detail: string, summary?: string, options?: any): void {
		this.notify('warn', detail, summary, options);
	}

	error(detail: string, summary?: string, options?: any): void {
		this.notify('error', detail, summary, options);
	}

	private notify(status: string, detail: string, summary?: string, options?: any): void {
		let opts = { duration: 2000, extraClasses: [status, status + '-snackbar'] };
		if (options) {
			opts = {...opts, ...options};
		}
		this.snackbar.open(detail, summary, opts);
	}

}
