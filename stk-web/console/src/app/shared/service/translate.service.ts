
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment/moment';
import 'moment/locale/it';
import 'moment/locale/en-gb';
import 'moment/locale/es';

/**
* This class provides the Translate service with methods to read names and add names.
*/
@Injectable()
export class TranslateServiceConfiguration {
    /**
    * Creates a new TranslateServiceConfiguration with the injected TranslateService
    * @param {TranslateService} TranslateService - The injected TranslateService.
    * @constructor
    */
    constructor(private translateService: TranslateService) { }

    setPreferredLanguage() {
        this.translateService.setDefaultLang('en');
        this.translateService.use('en');
        moment.locale(this.translateService.currentLang);
    }


    onLanguageChange() {
        this.translateService.onLangChange.take(1).subscribe((l) => {
            console.log(l.lang)
        })
    }

}
