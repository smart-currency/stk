import { NgModule, SkipSelf } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StkComponentsModule } from '@app/components';
import { SmartokenizerAppComponent } from '@app/stk.component';


const routes: Routes = [
    { path: '', redirectTo: '', pathMatch: 'full' },
    { path: '', component: SmartokenizerAppComponent },
];


@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: false })],
    exports: [RouterModule]
})
export class StkRoutingModule {}
