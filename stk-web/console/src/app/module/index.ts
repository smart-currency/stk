import { StkProjectsModule } from './projects/projects.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { StkSettingsModule } from '@app/module/settings/settings.module';
import { SharedModule } from '@app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { StkTransactionModule } from '@app/module/transaction/transaction.module';
import { StkWalletModule } from '@app/module/wallet/wallet.module';
import { StkContractModule } from '@app/module/contract/contract.module';
import { StkSidenavModule } from '@app/module/sidenav/sidenav.module';
import { ModuleWithProviders } from '@angular/compiler/src/core';
import { StkSearchModule } from '@app/module/search/search.module';
import { StkAccountModule } from '@app/module/account/account.module';
import { StkVersionComponent } from '@app/components/mat-dialog/version/version.component';
import { StkChartModule } from '@app/module/chart/chart.module';
import { StkUserModule } from '@app/module/user/user.module';
import { UserSettingsComponent } from '@app/components/mat-dialog/user-settings/user-settings.component';


const stkModule = [
    StkProjectsModule,
    StkSettingsModule,
    StkSidenavModule,
    StkTransactionModule,
    StkWalletModule,
    StkChartModule,
    StkSearchModule,
    StkAccountModule,
    StkContractModule,
    StkUserModule
]

//dialog entry components
const entryComponents = [
    StkVersionComponent,
    UserSettingsComponent
];

@NgModule({
    imports: [
        stkModule
    ],
    exports: [stkModule],
    declarations: [],
    entryComponents:[entryComponents],
    providers: [],
})
export class StkModule {
    public static forRoot(): ModuleWithProviders {
        return {
            ngModule: StkModule,
            providers: []
        };
    }
}
