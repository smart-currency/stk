import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { StkProjectsComponent } from '@app/module/projects/projects.component';


describe('StkProjectsComponent', () => {
  let component: StkProjectsComponent;
  let fixture: ComponentFixture<StkProjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StkProjectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StkProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
