import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { StkChartComponent } from '@app/module/chart/chart.component';

describe('StkChartComponent', () => {
  let component: StkChartComponent;
  let fixture: ComponentFixture<StkChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StkChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StkChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
