import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { StkTransactionComponent } from '@app/module/transaction/transaction.component';


describe('StkTransactionComponent', () => {
  let component: StkTransactionComponent;
  let fixture: ComponentFixture<StkTransactionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StkTransactionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StkTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
