import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterSidenavComponent } from './footer-sidenav.component';

describe('FooterSidenavComponent', () => {
  let component: FooterSidenavComponent;
  let fixture: ComponentFixture<FooterSidenavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterSidenavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterSidenavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
