import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { StkSearchComponent } from '@app/module/search/search.component';


describe('StkSearchComponent', () => {
  let component: StkSearchComponent;
  let fixture: ComponentFixture<StkSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StkSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StkSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
