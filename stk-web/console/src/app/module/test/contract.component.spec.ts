import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { StkContractComponent } from '@app/module/contract/contract.component';


describe('StkContractComponent', () => {
  let component: StkContractComponent;
  let fixture: ComponentFixture<StkContractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StkContractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StkContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
