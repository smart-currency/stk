import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { StkWalletComponent } from '@app/module/wallet/wallet.component';


describe('StkWalletComponent', () => {
  let component: StkWalletComponent;
  let fixture: ComponentFixture<StkWalletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StkWalletComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StkWalletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
