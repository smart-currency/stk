import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StkSettingsComponent } from '../settings/settings.component';

describe('SettingsComponent', () => {
  let component: StkSettingsComponent;
  let fixture: ComponentFixture<StkSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StkSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StkSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
