import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { StkAccountComponent } from '@app/module/account/account.component';


describe('AccountComponent', () => {
  let component: StkAccountComponent;
  let fixture: ComponentFixture<StkAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StkAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StkAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
