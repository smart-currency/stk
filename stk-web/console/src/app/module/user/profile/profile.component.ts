import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material';
import { UserSettingsComponent } from '@app/components/mat-dialog/user-settings/user-settings.component';

@Component({
  selector: 'stk-app-profile',
  templateUrl: './profile.component.html',
  encapsulation: ViewEncapsulation.None
})
export class UserProfileComponent {

  constructor( public dialog: MatDialog) { }


  openDialog(): void {
    let dialogRef = this.dialog.open(UserSettingsComponent, {
      id: 'user-settings-dialog',
      width: '35rem'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}
