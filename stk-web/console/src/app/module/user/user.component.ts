import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'stk-app-user',
  templateUrl: './user.component.html',
  encapsulation: ViewEncapsulation.None
})
export class UserComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
