import { ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';
import { RequireAuthGuard } from '@app/guard';
import { UserComponent } from '@app/module/user/user.component';

export const UserRoutesModule: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'user',
    component: UserComponent,
    canActivate: [RequireAuthGuard]
  }
]);
