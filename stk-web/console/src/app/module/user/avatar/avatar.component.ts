import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, Input } from '@angular/core';
import { ICountry } from '@stk/common/src/models';
import { TEST_AVATAR_URL } from '@stk/common/config/constant';

@Component({
  selector: 'stk-app-avatar',
  templateUrl: './avatar.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AvatarComponent implements OnInit {


  @Input() imageUrl:string
  
  constructor() { 
    this.imageUrl = TEST_AVATAR_URL
  }

  ngOnInit() {
  }

  

}


