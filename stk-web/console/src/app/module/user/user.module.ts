import { NgModule,ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from '@app/module/user/user.component';
import { UserProfileComponent } from '@app/module/user/profile/profile.component';
import { UserRoutesModule } from '@app/module/user/user.routing';
import { UserSettingsComponent } from '@app/components/mat-dialog/user-settings/user-settings.component';

const userComponents = [
  UserComponent,
  UserProfileComponent,
  UserSettingsComponent
]

@NgModule({
  imports: [
    CommonModule,
    UserRoutesModule
  ],
  declarations: [userComponents],
  exports:[userComponents]
})
export class StkUserModule {}
