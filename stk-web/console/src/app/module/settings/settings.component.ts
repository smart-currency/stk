import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'stk-app-settings',
  templateUrl: './settings.component.html',
  encapsulation: ViewEncapsulation.None
})
export class StkSettingsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
