import { ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';
import { RequireAuthGuard } from '@app/guard';
import { StkSettingsComponent } from '@app/module/settings/settings.component';

export const SettingsRoutesModule: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'settings',
    component: StkSettingsComponent,
    canActivate: [RequireAuthGuard]
  }
]);
