import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StkSettingsComponent } from './settings.component';
import { SettingsRoutesModule } from '@app/module/settings/settings.routing';

@NgModule({
  imports: [
    CommonModule,
    SettingsRoutesModule
  ],
  declarations: [StkSettingsComponent],
  exports:[StkSettingsComponent]
})
export class StkSettingsModule {}
