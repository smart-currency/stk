import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'stk-app-account',
  templateUrl: './account.component.html',
  encapsulation: ViewEncapsulation.None
})
export class StkAccountComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
