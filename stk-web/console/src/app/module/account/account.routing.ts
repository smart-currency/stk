import { ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';
import { RequireAuthGuard } from '@app/guard';
import { StkAccountComponent } from '@app/module/account/account.component';

export const AccountRoutesModule: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'accounts',
    component: StkAccountComponent,
    canActivate: [RequireAuthGuard]
  }
]);
