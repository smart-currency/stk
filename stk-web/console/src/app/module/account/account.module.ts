import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StkAccountComponent } from '@app/module/account/account.component';
import { AccountRoutesModule } from '@app/module/account/account.routing';

@NgModule({
  imports: [
    CommonModule,
    AccountRoutesModule
  ],
  declarations: [StkAccountComponent],
  exports:[StkAccountComponent]
})
export class StkAccountModule { }




