import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'stk-app-search',
  template: `<input aria-label="search" placeholder="Search" type="search">`,
  encapsulation: ViewEncapsulation.None,
  host:{
    class:'search-container'
  }
})
export class StkSearchComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
