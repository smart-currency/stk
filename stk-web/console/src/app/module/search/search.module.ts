import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StkSearchComponent } from '@app/module/search/search.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [StkSearchComponent],
  exports:[StkSearchComponent]
})
export class StkSearchModule { }
