import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectCardComponent } from './project-card/project-card.component';
import { ProjectRoutesModule } from '@app/module/projects/projects.routing';
import { StkProjectsComponent } from '@app/module/projects/projects.component';

@NgModule({
  imports: [
    CommonModule,
    ProjectRoutesModule
  ],
  declarations: [
    StkProjectsComponent,
    ProjectCardComponent
  ]
})
export class StkProjectsModule {
  
}
