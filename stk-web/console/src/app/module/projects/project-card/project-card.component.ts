import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'stk-app-project-card',
  templateUrl: './project-card.component.html',
  encapsulation: ViewEncapsulation.None
})
export class ProjectCardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
