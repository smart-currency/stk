import { ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';
import { RequireAuthGuard } from '@app/guard';
import { StkProjectsComponent } from './projects.component';

export const ProjectRoutesModule: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'projects',
    component: StkProjectsComponent,
    canActivate: [RequireAuthGuard]
  }
]);
