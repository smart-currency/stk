import { ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';
import { RequireAuthGuard } from '@app/guard';
import { StkWalletComponent } from './wallet.component';

export const WalletRoutesModule: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'wallet',
    component: StkWalletComponent,
    canActivate: [RequireAuthGuard]
  }
]);
