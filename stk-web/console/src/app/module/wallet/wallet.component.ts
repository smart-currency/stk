import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'stk-app-wallet',
  templateUrl: './wallet.component.html',
  encapsulation: ViewEncapsulation.None
})
export class StkWalletComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
