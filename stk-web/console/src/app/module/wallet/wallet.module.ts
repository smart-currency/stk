import { NgModule,ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StkWalletComponent } from '@app/module/wallet/wallet.component';
import { WalletRoutesModule } from '@app/module/wallet/wallet.routing';

@NgModule({
  imports: [
    CommonModule,
    WalletRoutesModule
  ],
  declarations: [StkWalletComponent]
})
export class StkWalletModule {}
