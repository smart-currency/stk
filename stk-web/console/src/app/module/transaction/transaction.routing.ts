import { ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';
import { RequireAuthGuard } from '@app/guard';
import { StkTransactionComponent } from './transaction.component';

export const TransactionRoutesModule: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'transaction',
    component: StkTransactionComponent,
    canActivate: [RequireAuthGuard]
  }
]);
