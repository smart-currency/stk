import { NgModule,ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StkTransactionComponent } from '@app/module/transaction/transaction.component';
import { TransactionRoutesModule } from '@app/module/transaction/transaction.routing';

@NgModule({
  imports: [
    CommonModule,
    TransactionRoutesModule
  ],
  declarations: [StkTransactionComponent],
  exports:[StkTransactionComponent]
})
export class StkTransactionModule {}
