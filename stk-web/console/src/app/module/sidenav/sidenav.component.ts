import { Component, OnInit, ViewEncapsulation, ViewChild, Input, Inject } from '@angular/core';
import { MatSidenav, MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material';
import { SidenavService } from '@app/module/sidenav/service/sidenav.service';
import { VersionService } from '../../api/version.service';
import { ENDPOINT } from '@stk/common/config/constant';
import { VersionModel } from '@stk/common/src';
import { StkVersionComponent } from '@app/components/mat-dialog/version/version.component';

@Component({
  selector: 'stk-app-sidenav',
  templateUrl: './sidenav.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [VersionService]
})
export class SidenavComponent implements OnInit {

  @ViewChild('sidenav')
  public sidenav: MatSidenav;

  @ViewChild('sidenavRight')
  public sidenavRight: MatSidenav;

  versionModel: VersionModel;
  version:string;

  constructor(private sidenavService: SidenavService, private versionService:VersionService,
    public dialog: MatDialog,
    ) { }

  ngOnInit(): void {
    this.sidenavService.setSidenav(this.sidenav);
    this.versionService.httpGet(ENDPOINT.VERSION).subscribe((v) => {
    this.versionModel = v.data;
    this.version = this.versionModel.appVersion
    })
  }
  openDialog(): void {
    let dialogRef = this.dialog.open(StkVersionComponent, {
      id: 'version-dialog',
      width: '35rem',
      data: (this.versionModel!==undefined) ? this.versionModel : ''
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}
