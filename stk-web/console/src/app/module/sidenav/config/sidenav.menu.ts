import { NavItem } from '@stk/common/src';

export const SidenavMenu: Array<NavItem> = [];


export const navMenu = {
  item: [
    {
      label: 'Smart Contract',
      link: 'contracts',
      icon: 'user',
    },
    {
      label: 'Account',
      link: 'accounts',
      icon: 'user',
    },
    {
      label: 'Wallet',
      link: 'wallet',
      icon: 'user',
    },
    {
      label: 'Transaction',
      link: 'transaction',
      icon: 'user',
    },
    {
        label: 'Android',
        link: 'contact',
        icon: 'android',
        hasTitleDivider:'Download App'
      },
      {
        label: 'IOS',
        link: 'contact',
        icon: 'ios',
    },
      {
        label: 'Electron',
        link: 'contact',
        icon: 'laptop',
    },
      {
        label: 'Impostazioni',
        link: 'contact',
        icon: 'settings',
        hasTitleDivider:'Settings'

    },
      {
        label: 'Guida',
        link: 'contact',
        icon: 'info-border',
    }
  ]
}.item.map((i) => SidenavMenu.push(i));



