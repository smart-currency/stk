import { NgModule,ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidenavComponent } from '@app/module/sidenav/sidenav.component';
import { SharedModule } from '@app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { StkNavMenuComponent } from '@app/module/sidenav/nav-menu/nav-menu.component';
import { MaterialModule } from '@app/shared/material/material.module';
import { StkNavItemComponent } from '@app/module/sidenav/nav-menu/nav-item.component';
import { SidenavExpanderComponent } from '@app/module/sidenav/expander/sidenav-expander.component';

const navComponents = [
  StkNavMenuComponent,
  StkNavItemComponent,
  SidenavExpanderComponent,
  SidenavComponent]

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule,
    SharedModule
  ],
  declarations: [navComponents],
  exports: [navComponents],
  bootstrap: []
})
export class StkSidenavModule { 
  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: StkSidenavModule,
      providers: []
    };
  }
}
