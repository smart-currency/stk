import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'stk-app-sidenav-expander',
  templateUrl: './sidenav-expander.component.html',
  encapsulation: ViewEncapsulation.None
})
export class SidenavExpanderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
