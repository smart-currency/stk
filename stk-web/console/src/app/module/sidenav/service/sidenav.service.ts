import { Injectable } from '@angular/core';
import { MatSidenav } from '@angular/material';

@Injectable()
export class SidenavService {

    private sidenav: MatSidenav;
    private rightSidenav: MatSidenav;

    public setSidenav(sidenav: MatSidenav) {
        this.sidenav = sidenav;
    }

    public setRightSidenav(sidenav: MatSidenav) {
        this.rightSidenav = sidenav;
    }

    public open() {
        return this.sidenav.open();
    }

    public close() {
        return this.sidenav.close();
    }

    public toggle(): void {
        this.sidenav.toggle();
    }
    public toggleRight(): void {
        this.rightSidenav.toggle();
    }
}
