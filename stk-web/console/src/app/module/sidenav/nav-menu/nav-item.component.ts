import { Component, Input, OnChanges,ViewEncapsulation, OnInit} from '@angular/core';
import { SidenavMenu } from '@app/module/sidenav/config/sidenav.menu';
import { NavItem } from '@stk/common/src';

@Component({
  selector: 'stk-app-nav-item',
  templateUrl: 'nav-item.component.html',
  encapsulation: ViewEncapsulation.None
})
export class StkNavItemComponent implements OnInit {
 

  navItem:Array<NavItem> = [];

  ngOnInit(): void {
    this.navItem = SidenavMenu
  }
  ngOnChanges() {
    
  }
}
