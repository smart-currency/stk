import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'stk-app-smart-contract',
  templateUrl: './contract.component.html',
  encapsulation: ViewEncapsulation.None
})
export class StkContractComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
