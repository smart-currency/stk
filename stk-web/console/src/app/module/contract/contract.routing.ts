import { ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';
import { RequireAuthGuard } from '@app/guard';
import { StkContractComponent } from './contract.component';

export const ContractRoutesModule: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'contracts',
    component: StkContractComponent,
    canActivate: [RequireAuthGuard]
  }
]);
