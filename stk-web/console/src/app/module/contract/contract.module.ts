import { NgModule,ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StkContractComponent } from '@app/module/contract/contract.component';
import { ContractRoutesModule } from '@app/module/contract/contract.routing';

@NgModule({
  imports: [
    CommonModule,
    ContractRoutesModule
  ],
  declarations: [StkContractComponent]
})
export class StkContractModule {}
