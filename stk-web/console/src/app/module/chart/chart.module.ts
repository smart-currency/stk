import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartRoutesModule } from '@app/module/chart/chart.routing';
import { StkChartComponent } from '@app/module/chart/chart.component';

@NgModule({
  imports: [
    CommonModule,
    ChartRoutesModule
  ],
  declarations: [StkChartComponent],
  
})
export class StkChartModule { }
