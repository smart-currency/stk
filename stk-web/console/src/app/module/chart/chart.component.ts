import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'stk-app-chart',
  templateUrl: './chart.component.html',
  encapsulation: ViewEncapsulation.None
})
export class StkChartComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
