import { ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';
import { RequireAuthGuard } from '@app/guard';
import { StkChartComponent } from '@app/module/chart/chart.component';

export const ChartRoutesModule: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'chart',
    component: StkChartComponent,
    canActivate: [RequireAuthGuard]
  }
]);
