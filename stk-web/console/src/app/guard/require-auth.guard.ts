import 'rxjs/add/operator/do';
import 'rxjs/add/operator/take';

import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { FirebaseAuthService } from '@app/shared/firebase/firebase.auth.service';


@Injectable()
export class RequireAuthGuard implements CanActivate {
  constructor(private auth: FirebaseAuthService, private router: Router) {}

  canActivate(): Observable<boolean> {
    return this.auth.authenticated$
      .take(1)
      .do(authenticated => {
        if (!authenticated) {
          this.router.navigate(['/console']);
        }
      });
  }
}
