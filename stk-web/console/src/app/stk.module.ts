import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ServiceWorkerModule } from '@angular/service-worker';
import { SmartokenizerAppComponent } from './stk.component';
import { StkComponentsModule } from '@app/components';
import { SERVICE_WORKER } from '@stk/common/config/constant';
import { SharedModule } from '@app/shared/shared.module';
import { StkRoutingModule } from '@app/stk.routing';
import { StkModule } from '@app/module';
import { SidenavService } from '@app/module/sidenav/service/sidenav.service';
import { RequireAuthGuard, RequireUnauthGuard } from '@app/guard';


@NgModule({
  declarations: [
    SmartokenizerAppComponent
  ],
  imports: [
    BrowserModule,
    StkRoutingModule,
    SharedModule.forRoot(),
    StkModule,
    StkComponentsModule,
    ServiceWorkerModule.register(SERVICE_WORKER.CONFIG, { enabled: true })//offline features
  ],
  providers: [SidenavService, RequireAuthGuard, RequireUnauthGuard],
  bootstrap: [SmartokenizerAppComponent]
})
export class SmartokenizerAppModule { }
