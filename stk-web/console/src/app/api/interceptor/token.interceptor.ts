import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { FirebaseAuthService } from '@app/shared/firebase/firebase.auth.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    
  constructor(public auth: FirebaseAuthService) {}
  
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    
    request = request.clone({
      setHeaders: {
        Authorization: `Smartokenizer ${this.auth.afAuth.idToken}`
      }
    });
    return next.handle(request);
  }
}
