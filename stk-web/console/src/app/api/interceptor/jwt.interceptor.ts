export interface JwtToken {
	contexts: string;
	exp: number;
	iat: number;
	iss: string;
	jti: string;
	scopes: string;
	sub: string;
}

import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { FirebaseAuthService } from '@app/shared/firebase/firebase.auth.service';


export class JwtInterceptor implements HttpInterceptor {
  constructor(public auth: FirebaseAuthService) {}
  intercept(request: HttpRequest<JwtToken>, next: HttpHandler): Observable<HttpEvent<JwtToken>> {
    
    return next.handle(request).do((event: HttpEvent<JwtToken>) => {
      if (event instanceof HttpResponse) {
        // do stuff with response if you want
      }
    }, (err: JwtToken) => {
      if (err instanceof HttpErrorResponse) {
        if (err.status === 401) {
          // redirect to the login route
          // or show a modal
        }
      }
    });
  }
}
