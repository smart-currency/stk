import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';
import { VersionModel } from '@stk/common/src/models';

/**
* This class provides the Version service with methods to read varsion and datas from db
*/
@Injectable()
export class VersionService extends BaseService<VersionModel> {
    /**
    * Creates a new VersionService with the injected HttpClient.
    * @param {Http} http - The injected HttpClient.
    * @constructor
    */
    constructor(http: HttpClient) {
        super(http)
    }

}
