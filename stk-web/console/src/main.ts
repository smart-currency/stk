import { enableProdMode, ApplicationRef } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { enableDebugTools } from '@angular/platform-browser';
import { SmartokenizerAppModule } from '@app/stk.module';
import { environment } from '@env/environment-dev';

if (environment.production) {
  enableProdMode();
}
  platformBrowserDynamic().bootstrapModule(SmartokenizerAppModule).then((module) => {
    let applicationRef = module.injector.get(ApplicationRef);
    let appSiteComponent = applicationRef.components[0];
    enableDebugTools(appSiteComponent);
  }).catch(err => console.log(err));
