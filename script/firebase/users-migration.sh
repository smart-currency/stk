#!/bin/bash
# migra tutti gli utenti del db locale in firebase, sotto il path relativo users
firebaseDatabaseUrl="https://smartokenizer.firebaseio.com"
firebaseCollectionPath="/users"
filePath="~/Desktop/"
fileName="output.json "


firebase-import --database_url $firebaseDatabaseUrl --path $firebaseCollectionPath --json $filePath$fileName 
