mongo --eval "db.stats()" 

RESULT=$?   # returns 0 if mongo eval succeeds

if [ $RESULT -ne 0 ]; then
    echo "mongodb not running. you must run  mongod!"
   
else
   echo "mongodb is running!\nStart Smartokenizer Server:\ngulp serve...\n"
    gulp serve
fi


