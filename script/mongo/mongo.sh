#stop mongo from terminal
mongo --eval "db.getSiblingDB('admin').shutdownServer()"

#from shell
mongo
use admin
db.shutdownServer()
