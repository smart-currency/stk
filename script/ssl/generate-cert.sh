#!/bin/bash

#comandi per generare certificati https in nodejs
mkdir ssl
rm /ssl/*
openssl genrsa -out ./ssl/smartokenizer-key.pem 1024
#valorizzare i campi richiesti
openssl req -new -key ./ssl/smartokenizer-key.pem -out ./ssl/certrequest.csr
openssl x509 -req -in ./ssl/certrequest.csr -signkey ./ssl/smartokenizer-key.pem -out ./ssl/smartokenizer-cert.pem
