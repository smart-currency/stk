#!/usr/bin/env bash
#clean all node_modules and other files
base_folder='/Users/christian/stk/script'
site_folder='stk-site'
web_folder='stk-web'

cd ..
cd $site_folder
rm -rf node_modules 
cd ..
cd $web_folder
cd smartokenizer
rm -rf node_modules
cd '/Users/christian/stk/script'
