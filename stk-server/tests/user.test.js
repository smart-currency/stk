import mongoose from 'mongoose'
import request from 'supertest-as-promised'
import httpStatus from 'http-status'
import chai, { expect } from 'chai'
import { app } from '../index'

chai.config.includeStack = true
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjoyLjM3NzExMzc0ODIyODA1NDcsImV4cCI6MTUyMDc4NjI3MiwiaWF0IjoxNTE5NTc2NjcyfQ.RQ4KpfpnLhIZ1sB63iGH_11gDOoWhLvQlSjlcGKzT_0'
const fakeId = '596e01ac5ac06e53db9d896b'

/**
 * root level hooks
 */
after((done) => {
  // required because https://github.com/Automattic/mongoose/issues/1251#issuecomment-65793092
  mongoose.models = {}
  mongoose.modelSchemas = {}
  mongoose.connection.close()
  done()
})

describe('## User APIs', () => {
  let user = {
    email: 'KK123',
    password: '250977cc'
  }

  describe('# POST /api/v1/user', () => {
    it('should create a new user', (done) => {
      request(app)
        .post('/api/v1/user')
        .set('x-access-token', token)
        .send(user)
        .then( () => {
          expect(httpStatus.OK)
          done()
        })
    })
  })

  describe('# GET /api/v1/user/:userId', () => {
    it('should get user details', (done) => {
      request(app)
        .get(`/api/v1/user/${fakeId}`)
        .set('x-access-token', token)
        .then( () => {
          expect(httpStatus.OK)
          done()
        })
    })
  })

  describe('# GET /api/v1/user/', () => {
    it('should get all users', (done) => {
      request(app)
        .get('/api/v1/user')
        .set('x-access-token', token)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.data).to.be.an('array')
          done()
        })
        .catch(done)
    })

    it('should get all users (with limit)', (done) => {
      request(app)
        .get('/api/v1/user')
        .set('x-access-token', token)
        .query({ limit: 10 })
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.data).to.be.an('array')
          done()
        })
        .catch(done)
    })
  })

  describe('# GET /api/v1/user/social', () => {
    it('should get user google profile', (done) => {
      request(app)
        .get(`/api/v1/user/${fakeId}/social`)
        .set('x-access-token', token)
        .query('q=google')
        .then( () => {
          expect(httpStatus.OK)
          done()
        })
    })

    it('should get user facebook profile', (done) => {
      request(app)
        .get(`/api/v1/user/${fakeId}/social`)
        .set('x-access-token', token)
        .query('q=facebook')
         .then( () => {
          expect(httpStatus.OK)
          done()
        })
        .catch(done)
    })

    it('should get user twitter profile', (done) => {
      request(app)
        .get(`/api/v1/user/${fakeId}/social`)
        .set('x-access-token', token)
        .query('q=twitter')
         .then( () => {
          expect(httpStatus.OK)
          done()
        })
        .catch(done)
    })

    it('should get request parameter error', (done) => {
      request(app)
        .get(`/api/v1/user/${fakeId}/social`)
        .set('x-access-token', token)
        .query('q=fake')
         .then( () => {
          expect(httpStatus.OK)
          done()
        })
        .catch(done)
    })

    it('should get user by Username', (done) => {
      request(app)
        .get('/api/v1/user/username')
        .query(`q=${user.email}`)
        .set('x-access-token', token)
        .then( () => {
          expect(httpStatus.OK)
          done()
        })
        .catch(done)
    })
  })

  describe('# GET /api/v1/user/:userId/donate', () => {
    it('should get donate response ', (done) => {
      request(app)
        .get(`/api/v1/user/${fakeId}/donate`)
        .query('emailTo=test&amount=2')
        .set('x-access-token', token)
        .then( () => {
          expect(httpStatus.OK)
          done()
        })
        .catch(done)
    })
  })

  describe('# GET /api/v1/user/:userId/donate', () => {
    it('should get donate response error 500 ', (done) => {
      request(app)
        .get(`/api/v1/user/${fakeId}/donate`)
        .set('x-access-token', token)
         .then( () => {
          expect(httpStatus.INTERNAL_SERVER_ERROR)
          done()
        })
        .catch(done)
    })
  })

  describe('# DELETE /api/v1/user/', () => {
    it('should delete user', (done) => {
      request(app)
        .delete(`/api/v1/user/${fakeId}`)
        .set('x-access-token', token)
         .then( () => {
          expect(httpStatus.OK)
          done()
        })
        .catch(done)
    })
  })
})
