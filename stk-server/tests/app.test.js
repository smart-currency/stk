import request from 'supertest-as-promised'
import httpStatus from 'http-status'
import jwt from 'jsonwebtoken'
import chai, { expect } from 'chai'
import { app } from '../index'
import config from '../config/config'

chai.config.includeStack = true

const authUrl = config.authApi
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjoyLjM3NzExMzc0ODIyODA1NDcsImV4cCI6MTUyMDc4NjI3MiwiaWF0IjoxNTE5NTc2NjcyfQ.RQ4KpfpnLhIZ1sB63iGH_11gDOoWhLvQlSjlcGKzT_0'

describe('## AppInfo APIs', () => {
  
    describe('# GET /api/v1/app/version', () => {
    it('should create a new token', (done) => {
      request(app)
        .get('/api/v1/app/version')
        .set('x-access-token',token)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.data.appVersion).to.equal(config.versionAPP)
          expect(res.body.data.apiVersion).to.equal(config.versionApi)
          done()
        })
        .catch(done)
    })
  })

})
