import mongoose from 'mongoose'
import request from 'supertest-as-promised'
import httpStatus from 'http-status'
import chai, { expect } from 'chai'
import { app } from '../index'

chai.config.includeStack = true
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjoyLjM3NzExMzc0ODIyODA1NDcsImV4cCI6MTUyMDc4NjI3MiwiaWF0IjoxNTE5NTc2NjcyfQ.RQ4KpfpnLhIZ1sB63iGH_11gDOoWhLvQlSjlcGKzT_0'
const fakeId = '596e01ac5ac06e53db9d896b'

/**
 * root level hooks
 */
after((done) => {
  // required because https://github.com/Automattic/mongoose/issues/1251#issuecomment-65793092
  mongoose.models = {}
  mongoose.modelSchemas = {}
  mongoose.connection.close()
  done()
})

describe('## Role Test APIs', () => {
  let role = {
    name: 'role test'
  }

  let role_put = {
    name: 'role updated'
  }

  describe('# GET /api/v1/role', () => {
    it('should return list of role', (done) => {
      request(app)
        .get('/api/v1/role')
        .set('x-access-token', token)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.data).to.be.an('array')
          done()
        })
        .catch(done)
    })
  })

  describe('# GET /api/v1/role/count', () => {
    it('should return the number of role is positive', (done) => {
      request(app)
        .get('/api/v1/role/count')
        .set('x-access-token', token)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(parseInt(res.body.data)).to.be.a('number')
          done()
        })
        .catch(done)
    })
  })

  describe('# POST /api/v1/role', () => {
    it('should create a new role', (done) => {
      request(app)
        .post('/api/v1/role')
        .set('x-access-token', token)
        .send(role)
        .then(() => {
          expect(httpStatus.OK)
          done()
        })
    })
  })

  describe('# GET /api/v1/role/:roleId', () => {
    it('should get role details', (done) => {
      request(app)
        .get(`/api/v1/role/${fakeId}`)
        .set('x-access-token', token)
        .then(() => {
          expect(httpStatus.OK)
          done()
        })
    })
  })

  describe('# PUT /api/v1/role/:roleId', () => {
    it(`should update a role`, (done) => {
      request(app)
        .put(`/api/v1/role/${fakeId}`)
        .set('x-access-token', token)
        .send(role_put)
        .then(() => {
          expect(httpStatus.OK)
          done()
        })
    })
  })

  describe('# DELETE /api/v1/role/:roleId', () => {
    it(`should delete a role`, (done) => {
      request(app)
        .delete(`/api/v1/role/${fakeId}`)
        .set('x-access-token', token)
        .then(() => {
          expect(httpStatus.OK)
          done()
        })
    })
  })
})
