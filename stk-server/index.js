import mongoose from 'mongoose';
import https from 'https';
import config from './config/config';
import credentials from './config/ssl';
import app from './config/express';


/** make bluebird default Promise **/
Promise = require('bluebird'); // eslint-disable-line no-global-assign

/** plugin bluebird promise in mongoose **/
mongoose.Promise = Promise;

/** connect to mongo db **/
const mongoUri = config.mongo.host;
const err = () => { throw new Error(`unable to connect to database: ${mongoUri}`); };
mongoose.connect(mongoUri, { server: { socketOptions: { keepAlive: 1 } } });
mongoose.connection.on('error', err);

/** print mongoose logs in dev env **/
/* istanbul ignore else: base method */
if (config.env === 'development') {
  mongoose.set('debug', true);
}

/** create https server for production **/
const productionServer = https.createServer(credentials, app);


/** module.parent check is required to support mocha watch **/
/** src: https://github.com/mochajs/mocha/issues/1912 **/
  /* istanbul ignore next: base method */
if (`${config.env}` === 'development') {
  app.listen(config.port, () => {
    console.info(`server started on port ${config.port} (${config.env}) and mongodb at host ${mongoUri} port ${config.mongo.port}`); // eslint-disable-line no-console
  });
} else {
  productionServer.listen(config.port, () => {
    console.info(`server started on port ${config.port} (${config.env}) and mongodb at host ${mongoUri} port ${config.mongo.port}`); // eslint-disable-line no-console
  });
}

export default { app, productionServer };
