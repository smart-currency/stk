import Joi from 'joi';
import env from './environment';

/** require and configure dotenv, will load vars in .env in PROCESS.ENV **/
require('dotenv').config();

/** define validation for all the env vars **/
const envVarsSchema = Joi.object({
  NODE_ENV: Joi.string().allow([env.dev, env.prod, env.test, env.provision]).default(env.dev),
  PORT: Joi.number().default(8000),
  PORT_HTTPS: Joi.number().default(8443),
  MONGOOSE_DEBUG: Joi.boolean()
    .when('NODE_ENV', {
      is: Joi.string().equal('development'),
      otherwise: Joi.boolean().default(false),
    }),
  JWT_SECRET: Joi.string().required().description('8a65944d-d3fb-46fc-a85e-0295c986cd9f'),
  MONGO_HOST: Joi.string().required().description('mongodb://localhost/smartokenizer'),
  MONGO_PORT: Joi.number().required().default(27017),
  REMOTE_HOST: Joi.string().default('www.vipzip.it/api'),
  REMOTE_PORT: Joi.number().default(8443),
  SMTP_HOST: Joi.string().default('smtp-out.kpnqwest.it'),
  SMTP_PORT: Joi.number().default(25),
  SMTP_AUTH_USER: Joi.string().default('noreply@smartokenizer.com'),
  SMTP_AUTH_PASS: Joi.string().default('noreplypw01'),
  SSL_KEY: Joi.string().default('ssl/smartokenizer-key.pem'),
  SSL_CERT: Joi.string().default('ssl/smartokenizer-cert.pem'),
  SSL_CSR: Joi.string().default('ssl/certrequest.crs'),
  FIREBASE_DB: Joi.string().default('https://smartokenizer-1ec08.firebaseio.com/'),
  VERSION_API: Joi.string().default('v1'),
  VERSION_APP: Joi.string().default('1.4'),
  URL_API: Joi.string().default('/v1/'),
}).unknown().required();

const { error, value: envVars } = Joi.validate(process.env, envVarsSchema);

/* istanbul ignore else: base method */
if (error) {
  throw new Error(`Config validation error: ${error.message}`);
}

const currentPort = (envVars.NODE_ENV === env.dev)
                  ? envVars.PORT
                  : envVars.PORT_HTTPS;

const config = {
  env: envVars.NODE_ENV,
  port: currentPort,
  mongooseDebug: envVars.MONGOOSE_DEBUG,
  jwtSecret: envVars.JWT_SECRET,
  mongo: {
    host: envVars.MONGO_HOST,
    port: envVars.MONGO_PORT,
  },
  domain: {
    host: envVars.REMOTE_HOST,
    port: envVars.REMOTE_PORT,
  },
  ssl: {
    key: envVars.SSL_KEY,
    cert: envVars.SSL_CERT,
    scr: envVars.SSL_CSR,
  },
  mail: {
    host: envVars.SMTP_HOST,
    port: envVars.SMTP_PORT,
    auth: {
      user: envVars.SMTP_AUTH_USER,
      pass: envVars.SMTP_AUTH_PASS,
    },
  },
  versionApi: envVars.VERSION_API,
  versionAPP: envVars.VERSION_APP,
  firebaseDatabaseUrl: envVars.FIREBASE_DB,
  authHeaders: envVars.AUTH_HEADER,
  urlApi: envVars.URL_API,
  authApi: ''.concat(envVars.URL_API).concat('/auth'),
  userApi: ''.concat(envVars.URL_API).concat('/users'),
};

export default config;

