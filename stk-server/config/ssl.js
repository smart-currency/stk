import fs from 'fs';
import config from './config';

const key = fs.readFileSync(config.ssl.key);
const cert = fs.readFileSync(config.ssl.cert);

const credentials = {
  key: key,
  cert: cert,
};

export default credentials;
