import jwt from 'jsonwebtoken';
import config from './config';
import { UnauthorizeAPIResponse, ForbiddenAPIResponse } from '../api/helpers/APIResponse';

const AuthenticatedMiddleware = (req, res, next) => {
  const request = req;
  const token = request.headers[config.authHeaders];

  if (token) {
    jwt.verify(token, config.jwtSecret, (err, decodeToken) => {
      if (err) {
        return res.status(401).json(new UnauthorizeAPIResponse(request, 'Failed to authenticate. Invalid token.'));
      }
      request.decoded = decodeToken;
      return next();
    });
  } else {
    return res.status(403).json(new ForbiddenAPIResponse(request, 'No token provided.'));
  }
  return true;
};

export default AuthenticatedMiddleware;
