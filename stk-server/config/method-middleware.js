import httpError from 'http-errors';

let methodsArray = [];

const toUpperCase = (str) => {
  const strNew = str.toUpperCase();
  return strNew;
};

const normalizeAllowedMethods = (methods) => {
  methodsArray = Array.prototype.slice.call(methods);
  return methodsArray.map(toUpperCase);
};


const MethodMiddleware = {
  allowMethods: (methods = {}, message) => {
    methods = normalizeAllowedMethods(methods || []); // eslint-disable-line no-param-reassign
    return (request, response, next) => {
      if (methods.indexOf(request.method.toUpperCase()) === -1) {
        response.header('Allow', methods.join(', '));
        return next(httpError(405, message));
      }
      return next();
    };
  },
};

export default MethodMiddleware;
