const env = {
  dev: 'development',
  prod: 'production',
  test: 'test',
  provision: 'provision',
};

export default env;
