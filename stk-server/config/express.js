import cors from 'cors';
import helmet from 'helmet';
import logger from 'morgan';
import express from 'express';
import compress from 'compression';
import httpStatus from 'http-status';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import methodOverride from 'method-override';
import expressWinston from 'express-winston';
import expressValidation from 'express-validation';
import env from './environment';
import config from './config';
import routes from '../api/routes/index.route';
import { APIError } from '../api/helpers/APIError';
import winstonInstance from './winston';
import {
  UnauthorizeAPIResponse,
  BadRequestAPIResponse,
  ForbiddenAPIResponse,
  NotFoundAPIResponse,
  MethodNotAllowedAPIResponse,
  InternalServerErrorAPIResponse,
} from '../api/helpers/APIResponse';

const app = express();

/* istanbul ignore else: base method */
if (config.env === env.dev) {
  app.use(logger('dev'));
}

/** parse body params and attache them to req.body **/
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cookieParser());
app.use(compress());
app.use(methodOverride());

/** secure apps by setting various HTTP headers **/
app.use(helmet());

/** enable CORS - Cross Origin Resource Sharing **/
app.use(cors());


/** enable detailed API logging in dev env **/
/* istanbul ignore else: base method */
if (config.env === 'development' || config.env === 'production') {
  expressWinston.requestWhitelist.push('body');
  app.use(expressWinston.logger({
    winstonInstance,
    meta: true, // optional: log meta data about request (defaults to true)
    msg: 'HTTP {{req.method}} {{req.url}} {{res.statusCode}} {{res.responseTime}}ms',
    colorStatus: false, // Color the status code (default green, 3XX cyan, 4XX yellow, 5XX red).
    colorize: false,
  }));
}

/** mount all routes on /api/versionApi path **/
app.use(`/api/${config.versionApi}`, routes);

/** if error is not an instanceOf APIError, convert it. **/
 /* istanbul ignore next */
app.use((err, req, res, next) => {
  /* istanbul ignore next: base method */
  if (err instanceof expressValidation.ValidationError) {
    /** validation error contains errors which is an array of error each containing message[] **/
    const unifiedErrorMessage = err.errors.map(error => error.messages.join('. ')).join(' and ');
    const error = new APIError(unifiedErrorMessage, err.status, true);
    return next(error);
  } else if (!(err instanceof APIError)) {
    const apiError = new APIError(err.message, err.status, err.isPublic);
    return next(apiError);
  }/* istanbul ignore else: base method */
  return next(err);
});

/** catch 404 and forward to error handler **/
app.use((req, res, next) => {
  const err = new APIError('API Not Found', httpStatus.NOT_FOUND);
  return next(err);
});

/** log error in winston transports except when executing test suite **/
 /* istanbul ignore next */
if (config.env !== 'test') {
  app.use(expressWinston.errorLogger({
    winstonInstance,
  }));
}/* istanbul ignore else: base method */

/** error handler **/
app.use((err, req, res, next) => { // eslint-disable-line no-unused-vars
  switch (err.status) {
    case 400: res.status(400).json(new BadRequestAPIResponse(req, err.message)); break;
    case 401: res.status(401).json(new UnauthorizeAPIResponse(req)); break;
    case 403: res.status(403).json(new ForbiddenAPIResponse(req)); break;
    case 404: res.status(404).json(new NotFoundAPIResponse(req, null, err.message)); break;
    case 405: res.status(405).json(new MethodNotAllowedAPIResponse(req)); break;
    case 500: res.status(500).json(new InternalServerErrorAPIResponse(req, err.message)); break;
    default: res.json(new InternalServerErrorAPIResponse(req, err.message)); break;
  }
});

export default app;
