<center>[![N|Solid](http://smartokenizer.org/smartokenizer.svg)](http://smartokenizer.org/)
</center>
# SmarTokenizer

>Any developer working on the repository should not modify the master and develop branch altogether. Everyone creates their own branch and can work without problems, doing their own tests.

### Stack of Technolgy used

* [Angular](https://angular.io): Angular 5
* [Angular Material](https://material.angular.io): Angular Material 2 (For Angular version >= 2)
* [Firebase](https://firebase.google.com): Firebase Cloud Service
* [Bebel](https://babeljs.io/): ES6 support using Babel (traspiler).
* [Bebel](https://babeljs.io/): ES6 support using Babel.
* [JWT](https://jwt.io): Authentication via JsonWebToken
* [ESLINT](http://eslint.org/): Code Linting
* [Instanbul](https://www.npmjs.com/package/istanbul): Supports code coverage of ES6 code using istanbul and mocha and statistic in html.
* [Nodemon](https://github.com/remy/nodemon): Restart the server using nodemon in real-time anytime an edit is made, with babel compilation and eslint.
* [Yarn](https://yarnpkg.com/): Uses new released yarn package manager by facebook, in rplace of npm. (below the difference)
* [MongoDB](https://www.mongodb.com/): MongoDB as NOSQL database.
* [Gulp](http://gulpjs.com): task runner.
* [ApiDocs](http://apidocjs.com): For create API Documentation via npm.
* [NodeJS](https://nodejs.org/it/): server with express and https.
* [Docker](https://www.docker.com):for build app in container virtualization.

>get started

### Prerequisites ###

The Project requires:

* [NodeJS](https://nodejs.org/) version >=4.8.0 to run.
* [Npm](http://npmjs.com) version >=2.15.11 to run.
* [Yarn](https://yarnpkg.com/) version >=0.20.3 to run.
* [MongoDB](https://www.mongodb.com/): an istance of MongoDB must be run.
* [Gulp](http://gulpjs.com): You can install via npm so: $ npm install -g gulp && npm install -g gulp-cli
* [Postman](https://www.getpostman.com): to test api.


### Yarn vs Npm ###
 > In npm, the npm  `shrinkwrap` command generates a lock file as well, and npm install reads that file before reading package.json, much like how Yarn reads yarn.lock first. The important difference here is that **Yarn  always creates and updates yarn.lock**, while npm doesn’t create one by default and only updates npm-shrinkwrap.json when it exists.
 
1. yarn more velocity

### Installation
Clone Gilab repository:

```sh
$ git clone https://gitlab.com/onedigitalsolution/smartokenizer
$ cd smartokenizer
$ cd server
$ npm install -g yarn
$ yarn install
```

Start the server:

```sh
$ yarn start   [for http server, development mode]
```
open [Postman](https://www.getpostman.com) and test API. At the moment ther are 5 API for route:

* `GET /` - retrieve all document;
* `POST /` - create a document;
* `GET /:id` - retrive document by id;
* `PUT /:id` - update a document;
* `DELETE /:id` - delete a document;
* `GET /count` - retrive a numnber of document;

> route:

* [http://localhost:8000/api/v1/](http://localhost:8000/api/v1/)
* [http://localhost:8000/api/v1/auth/token](http://localhost:8000/api/v1/auth/random-number). Here before you must create an user (Must rise Unauthorized).
* [http://localhost:8000/api/v1/user](http://localhost:8000/api/v1/user)
* [http://localhost:8000/api/v1/session](http://localhost:8000/api/v1/session)
* [http://localhost:8000/api/v1/transaction](http://localhost:8000/api/v1/transaction)
* [http://localhost:8000/api/v1/wallet](http://localhost:8000/api/v1/wallet)
* [http://localhost:8000/api/v1/crypto](http://localhost:8000/api/v1/crypto)


if you want use the https in local environemnt must run this command:

```sh
$ yarn gen:cert
$ yarn start:ssl
```
open [Postman](https://www.getpostman.com) and test some API:

* [https://localhost:8443/api/](https://localhost:8000/api/v1)
* [https://localhost:8443/api/users](https://localhost:8000/api/v1/user)
* [https://localhost:8443/api/auth/token](https://localhost:8000/api/v1/auth/token). Here before you must create an user.



Tests:
```sh
# Run tests written in ES6 
$ yarn test

# Run test along with code coverage
$ yarn test:coverage

# Run tests on file change
$ yarn test:watch

# Run tests enforcing code coverage (configured via .istanbul.yml)
yarn test:check-coverage
```

Lint:
```sh
# Lint code with ESLint
$ yarn lint

# Run lint on any file change
$ yarn lint:watch
```

Other gulp tasks:
```sh
# Wipe out dist and coverage directory
$ gulp clean

# Default task: Wipes out dist and coverage directory. Compiles using babel.
$ gulp
```

### Work in Local Environment
For created new branch to work in local, after installation, run:

```sh
# create branch
$ git checkout -b your-branch-name
# push the branch just created online, in bitbucket cloud solution
$ git push --set-upstream origin my-branch
# each new modify lunch
$ git add --all
$ git commit -m "message commit"
$ git push
```

#### Project File Structure

- `/server` - root folder
    - `/api` - the main folder for Express Serve
        - `/constant` - folder for constant esports as module
        - `/controllers` - controller to use in Express Server
        - `/helpers` - Utils, custom libraries, functions
        - `/i18n` - Internationalization (multi-language handler)
        - `/models` - data model of the project for `mongodb` mapping
        - `/routes` - Express Router route
        - `/validation` - custom route validation for model property
    - `/config` - App-wide variables and config
    - `/script` - `bash` script for development
    - `/ssl` - HTTPS Certified
    - `/tests` - test for all the files in the project

#### Deployment

```sh
# compile to ES5
$ yarn build

# upload dist/ to the smartokenizer.com server (in progress)
$ scp -rp dist/ user@dest:/path

# install production dependencies only
$ yarn --production

# Use any process manager to start your services
$ pm2 start dist/index.js or
$ node dist/index.js
```

In production you need to make sure your server is always up so you should ideally use any of the process manager recommended [here](http://expressjs.com/en/advanced/pm.html).
We recommend [pm2](http://pm2.keymetrics.io/) as it has several useful features like it can be configured to auto-start your services if system is rebooted.

## Logging

Universal logging library [winston](https://www.npmjs.com/package/winston) is used for logging. It has support for multiple transports.  A transport is essentially a storage device for your logs. Each instance of a winston logger can have multiple transports configured at different levels. For example, one may want error logs to be stored in a persistent remote location (like a database), but all logs output to the console or a local file. We just log to the console for simplicity, you can configure more transports as per your requirement.

### API logging ###
Logs detailed info about each api request to console during development.

![Detailed API logging](https://cloud.githubusercontent.com/assets/4172932/12563354/f0a4b558-c3cf-11e5-9d8c-66f7ca323eac.JPG)

#### Error logging ###
Logs stacktrace of error to console along with other details. You should ideally store all error messages persistently.

![Error logging](https://cloud.githubusercontent.com/assets/4172932/12563361/fb9ef108-c3cf-11e5-9a58-3c5c4936ae3e.JPG)

### Code Coverage ###
Get code coverage summary on executing `yarn test`


![Code Coverage Text Summary](https://cloud.githubusercontent.com/assets/4172932/12827832/a0531e70-cba7-11e5-9b7c-9e7f833d8f9f.JPG)

`yarn test:coverage` also generates HTML code coverage report in `coverage/` directory. Open `lcov-report/index.html` to view it.


![Code coverage HTML report](https://cloud.githubusercontent.com/assets/4172932/12625331/571a48fe-c559-11e5-8aa0-f9aacfb8c1cb.jpg)

## API Documentation ##
To create API documentation, first of all we can documentation the code and this are the command to generate:

```sh
# generate API doc
$ yarn api:docs
# open API doc (for mac only, i believe, otherwise open docs/index.html)
$ yarn apidocs:open
# push API documentation on remote server (vipzip.it)
$ yarn api:release
```

## Docker (work in progress, and for mac)

```sh
# For Development
$ yarn docker:dev
# For Test
$ yarn docker:test
	```

## Continuos Integration with Pipeline Gitlab 

 * work in progress on branch master, and for each commit start a pipeline Bitbucket for build the entirely project.

## Issue
 * for features or modifications open issue [here](https://gitlab.com/onedigitalsolution/smartokenizer/issues?status=new&status=open)

## License
This project is licensed under the [SmarTokenizer License]()

## Meta Info

SmarTokenizer Team

* [GiHub](https://github.com/chrchm) : @smartokenizer
* [Email](mailto:c.chiama@icloud.com) : a.paperini@gmail.com

