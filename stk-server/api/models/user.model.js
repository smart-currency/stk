import mongoose from 'mongoose';
import bcrypt from 'bcryptjs';
import Constants from '../constant/model.constant';
import BaseSchema from '../common/base.model';

require('mongoose-double')(mongoose); // eslint-disable-line func-names

const SchemaTypes = mongoose.Schema.Types;

const ProfileSchema = new mongoose.Schema({
  displayName: {
    type: String,
    required: false,
    default: Constants.VALUE.STRING,
  },
  email: {
    type: String,
    required: false,
    default: Constants.VALUE.STRING,
    unique: true,
  },
  password: {
    type: String,
    required: false,
    default: Constants.VALUE.STRING,
  },
  phoneNumber: {
    type: String,
    required: false,
    default: Constants.VALUE.STRING,
  },
  photoUrl: {
    type: String,
    required: false,
    default: Constants.VALUE.AVATAR_DEFAULT,
  },
}, { _id: false });


/**
 * User Schema
 */
const UserSchema = new BaseSchema({
  uid: {
    type: String,
  },
  local: ProfileSchema,
  accessToken: {
    type: String,
    default: Constants.EMPTY_STRING,
  },
  refreshToken: {
    type: String,
    default: Constants.EMPTY_STRING,
  },
  wallet: {
    type: Array,
    default: [
      {
        source: Constants.EMAIL_DEFAULT,
        share: 100,
      },
    ],
  },
  profile: {
    state: {
      type: String,
      default: Constants.STATE_DEFAULT,
    },
    region: {
      type: String,
      default: Constants.EMPTY_STRING,
    },
    province: {
      type: String,
      default: Constants.EMPTY_STRING,
    },
    city: {
      type: String,
      default: Constants.EMPTY_STRING,
    },
    language: {
      type: String,
      default: Constants.LANGUAGE_DEFAULT,
    },
  },
  facebook: ProfileSchema,
  twitter: ProfileSchema,
  google: ProfileSchema,
  roles: [{
    type: SchemaTypes.ObjectId,
    ref: Constants.MODEL.ROLE,
  }],
  createAt: {
    type: Date,
    default: Date.now,
  },
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */
UserSchema.pre('save', (next) => {
  if (this.local.password) {
    const salt = bcrypt.genSaltSync(Constants.METHOD.BCRYPT_SALT_VALUE);
    this.local.password = bcrypt.hashSync(this.local.password, salt);
  }
  if (this.roles.length === 0) {
    this.roles.push(new mongoose.Types.ObjectId('5962a5f37bde228394da6f72'));
  }
  next();
});

/**
 * @typedef User
 */
export default mongoose.model(Constants.MODEL.USER, UserSchema);
