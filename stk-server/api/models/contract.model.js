import mongoose from 'mongoose';
import Constants from '../constant/model.constant';
import BaseSchema from '../common/base.model';
/**
 * Contract Schema
 */
const ContractSchema = new BaseSchema({
  name: {
    type: String,
    unique: true,
  },
  created: {
    type: Date,
    default: Date.now,
  },
});

/**
 * @typedef CONTRACT
 */
export default mongoose.model(Constants.MODEL.CONTRACT, ContractSchema);

