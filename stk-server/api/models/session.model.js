import mongoose from 'mongoose';
import Constants from '../constant/model.constant';
import BaseSchema from '../common/base.model';

/** fix for SchemaTypes.double value **/
require('mongoose-double')(mongoose); // eslint-disable-line func-names

/**
 * Session Schema
 */
const SessionSchema = new BaseSchema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: Constants.MODEL.USER,
    unique: true,
  },
  createAt: {
    type: Date,
    default: Date.now,
  },
});

/**
 * @typedef Message
 */
export default mongoose.model(Constants.MODEL.SESSION, SessionSchema);
