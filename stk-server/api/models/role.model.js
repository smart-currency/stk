import mongoose from 'mongoose';
import Constants from '../constant/model.constant';
import BaseSchema from '../common/base.model';
/**
 * Role Schema
 */
const RoleSchema = new BaseSchema({
  name: {
    type: String,
    unique: true,
  },
  created: {
    type: Date,
    default: Date.now,
  },
});

/**
 * @typedef Message
 */
export default mongoose.model(Constants.MODEL.ROLE, RoleSchema);

