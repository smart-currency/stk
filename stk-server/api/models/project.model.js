import mongoose from 'mongoose';
import Constants from '../constant/model.constant';
import BaseSchema from '../common/base.model';
/**
 * Project Schema
 */
const ProjectSchema = new BaseSchema({
  name: {
    type: String,
    unique: true,
  },
  created: {
    type: Date,
    default: Date.now,
  },
});

/**
 * @typedef PROJECT
 */
export default mongoose.model(Constants.MODEL.PROJECT, ProjectSchema);

