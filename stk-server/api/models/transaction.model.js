import mongoose from 'mongoose';
import Constants from '../constant/model.constant';
import BaseSchema from '../common/base.model';
/**
 * Role Schema
 */
const TransactionSchema = new BaseSchema({
  name: {
    type: String,
    unique: true,
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: Constants.MODEL.USER,
  },
  created: {
    type: Date,
    default: Date.now,
  },
});

/**
 * @typedef Message
 */
export default mongoose.model(Constants.MODEL.TRANSACTION, TransactionSchema);

