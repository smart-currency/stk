import Project from '../models/project.model';
import BaseController from '../common/base.controller';
import { OkDataResponse, NotFoundAPIResponse } from '../helpers/APIResponse';

export default class ProjectController extends BaseController {
  constructor(model) {
    super(model);

    /**
     * Load Project and append to req.
     */
    this.load = (req, res, next, id) => {
      /* istanbul ignore next */
      Project.get(id)
        .then((Project) => {
          if (!Project) return res.status(404).json(new NotFoundAPIResponse(req, this.modelName.toLowerCase()));
          req.project = project; // eslint-disable-line no-param-reassign
          return next();
        })
        .catch(() => res.status(404).json(new NotFoundAPIResponse(req, this.modelName.toLowerCase())));
    };
    /**
     * Get Project
     * @returns {Project}
     */
    this.get = (req, res) => {
      res.json(new OkDataResponse(req, `Project with id ${req.project.id}`, req.project));
    };

    /**
     * Create new Project
     * @property {string} req.body.name - The name of Project.
     * @returns {Project}
     */
    this.create = (req, res, next) => {
      const project = new Project({
        name: req.body.name,
      });
      /* istanbul ignore next */
      project.save()
        .then(p => res.json(new OkDataResponse(req, 'Project cretaed with success.', p)))
        .catch(e => next(e));
    };

    /**
     * Update existing Project
     * @property {string} req.body.name - The secret of Project.
     * @returns {Project}
     */
    this.update = (req, res, next) => {
      const Project = req.project;
      project.name = req.body.name;
      /* istanbul ignore next */
      project.save()
        .then(p => res.json(new OkDataResponse(req, 'Project updated with success.', p)))
        .catch(e => next(e));
    };

    /**
     * Delete Project.
     * @returns {Project}
     */
    this.remove = (req, res, next) => {
      const project = req.Project;
      /* istanbul ignore next */
      project.remove()
        .then(p => res.json(new OkDataResponse(req, 'Project deleted with success.', p)))
        .catch(e => next(e));
    };
  }
}
