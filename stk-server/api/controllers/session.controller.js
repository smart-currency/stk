import Session from '../models/session.model';
import BaseController from '../common/base.controller';
import { OkDataResponse, NotFoundAPIResponse } from '../helpers/APIResponse';

export default class SessionController extends BaseController {
  constructor(model) {
    super(model);

    /**
     * Load Session and append to req.
     */
    this.load = (req, res, next, id) => {
      /* istanbul ignore next */
      Session.get(id)
        .then((s) => {
          if (!s) return res.status(404).json(new NotFoundAPIResponse(req, this.modelName.toLowerCase()));
          req.session = s; // eslint-disable-line no-param-reassign
          return req;
        })
        .catch(() => res.status(404).json(new NotFoundAPIResponse(req, this.modelName.toLowerCase())));
    };
    /**
     * Get Session
     * @returns {Session}
     */
    this.get = (req, res) => {
      res.json(new OkDataResponse(req, `Session with id ${req.session.id}`, req.session));
    };

    /**
     * Create new Session
     * @property {string} req.body.name - The name of Session.
     * @returns {Session}
     */
    this.create = (req, res, next) => {
      const session = new Session({
        name: req.body.name,
      });
      /* istanbul ignore next */
      session.save()
        .then(s => res.json(new OkDataResponse(req, 'Session cretaed with success.', s)))
        .catch(e => next(e));
    };

    /**
     * Update existing Session
     * @property {string} req.body.name - The secret of Session.
     * @returns {Session}
     */
    this.update = (req, res, next) => {
      const session = req.session;
      /* istanbul ignore next */
      session.save()
        .then(s => res.json(new OkDataResponse(req, 'Session updated with success.', s)))
        .catch(e => next(e));
    };

    /**
     * Delete Session.
     * @returns {Session}
     */
    this.remove = (req, res, next) => {
      const session = req.session;
      /* istanbul ignore next */
      session.remove()
        .then(s => res.json(new OkDataResponse(req, 'Session deleted with success.', s)))
        .catch(e => next(e));
    };
  }
}
