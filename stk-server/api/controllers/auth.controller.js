import jwt from 'jsonwebtoken';
import config from '../../config/config';
import { OkDataResponse, BadRequestAPIResponse } from '../helpers/APIResponse';

const request = {
  client_id: null,
  client_secret: null,
  scope: null,
};

const response = {
  access_token: '',
  expires_in: 1209600,
  token_type: 'jwt',
  refresh_token: '',
  grant_type: 'access',
};

export default class AuthController {
  constructor() {
    this.jwt = jwt;
  }

  createToken = (req, res) => {
    const token = this.jwt.sign({
      data: (Math.random(1, 1000) * 25) % 6,
      exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 14),
    }, config.jwtSecret);

    const refreshToken = this.jwt.sign({
      data: (Math.random(1, 100) * 5) % 6,
      exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 14),
    }, config.jwtSecret);

    response.access_token = token;
    response.refresh_token = refreshToken;
    return res.json(new OkDataResponse(req, 'Authentication credentials', response));
  }

  authorize = (req, res) => {
    if (!req.body.client_id) return res.status(400).json(new BadRequestAPIResponse(req, 'client_id'));
    if (!req.body.client_secret) return res.status(400).json(new BadRequestAPIResponse(req, 'client_secret'));
    request.client_id = req.body.client_id;
    request.client_secret = req.body.client_secret;
    request.scope = 'access';

    const token = this.jwt.sign({
      data: request,
      exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 14),
    }, config.jwtSecret);

    const refreshToken = this.jwt.sign({
      data: request,
      exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 14),
    }, config.jwtSecret);

    response.access_token = token;
    response.refresh_token = refreshToken;

    return res.json(new OkDataResponse(req, 'Authentication Data', response));
  }
}
