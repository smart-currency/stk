import Wallet from '../models/wallet.model';
import BaseController from '../common/base.controller';
import { OkDataResponse, NotFoundAPIResponse } from '../helpers/APIResponse';

export default class WalletController extends BaseController {
  constructor(model) {
    super(model);

    /**
     * Load Wallet and append to req.
     */
    this.load = (req, res, next, id) => {
      /* istanbul ignore next */
      Wallet.get(id)
        .then((s) => {
          if (!s) return res.status(404).json(new NotFoundAPIResponse(req, this.modelName.toLowerCase()));
          req.wallet = s; // eslint-disable-line no-param-reassign
          return next();
        })
        .catch(() => res.status(404).json(new NotFoundAPIResponse(req, this.modelName.toLowerCase())));
    };
    /**
     * Get Wallet
     * @returns {Wallet}
     */
    this.get = (req, res) => {
      res.json(new OkDataResponse(req, `Wallet with id ${req.wallet.id}`, req.wallet));
    };

    /**
     * Create new Wallet
     * @property {string} req.body.name - The name of Wallet.
     * @returns {Wallet}
     */
    this.create = (req, res, next) => {
      const wallet = new Wallet({
        name: req.body.name,
      });
      /* istanbul ignore next */
      wallet.save()
        .then(s => res.json(new OkDataResponse(req, 'Wallet cretaed with success.', s)))
        .catch(e => next(e));
    };

    /**
     * Update existing Wallet
     * @property {string} req.body.name - The secret of Wallet.
     * @returns {Wallet}
     */
    this.update = (req, res, next) => {
      const wallet = req.wallet;
      /* istanbul ignore next */
      wallet.save()
        .then(s => res.json(new OkDataResponse(req, 'Wallet updated with success.', s)))
        .catch(e => next(e));
    };

    /**
     * Delete Wallet.
     * @returns {Wallet}
     */
    this.remove = (req, res, next) => {
      const wallet = req.wallet;
      /* istanbul ignore next */
      wallet.remove()
        .then(s => res.json(new OkDataResponse(req, 'Wallet deleted with success.', s)))
        .catch(e => next(e));
    };
  }
}
