import User from '../models/user.model';
import BaseController from '../common/base.controller';
import { OkDataResponse, NotFoundAPIResponse } from '../helpers/APIResponse';

export default class UserController extends BaseController {
  constructor(model) {
    super(model);

    this.getRoleById = (req, res) => {
      const user = req.user;
      const roles = user.roles;
      return res.json(new OkDataResponse(req, 'User roles', roles));
    };
    /**
     * Load user and append to req.
     */
    this.load = (req, res, next, id) => {
      /* istanbul ignore next */
      User.findById(id)
        .populate('roles')
        .then((user) => {
          if (!user) return res.status(404).json(new NotFoundAPIResponse(req, this.modelName.toLowerCase()));
          req.user = user; // eslint-disable-line no-param-reassign
          return next();
        })
        .catch(() => res.status(404).json(new NotFoundAPIResponse(req, this.modelName.toLowerCase())));
    };

    /**
     * Get user
     * @returns {User}
     */
    this.get = (req, res) => {
      res.json(new OkDataResponse(req, `User with id ${req.user.id}`, req.user));
    };

    /**
     * Create new user
     * @property {string} req.body.email - The email of user.
     * @property {string} req.body.mobileNumber - The mobileNumber of user.
     * @returns {User}
     */

    /**
     *
     * @param {*} req
     * @param {*} res
     * @param {*} next
     */
    this.findAll = (req, res, next) => {
      const limit = (req.query.limit) ? req.query.limit : 15;
      User.find({})
        .populate('roles')
        .limit(parseInt(limit))
        .then((u) => {
          res.json(new OkDataResponse(req, 'All users founded.', u));
        })
        .catch(e => next(e));
    };
  }
}
