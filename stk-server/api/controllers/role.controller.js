import Role from '../models/role.model';
import BaseController from '../common/base.controller';
import { OkDataResponse, NotFoundAPIResponse } from '../helpers/APIResponse';

export default class RoleController extends BaseController {
  constructor(model) {
    super(model);

    /**
     * Load Role and append to req.
     */
    this.load = (req, res, next, id) => {
      /* istanbul ignore next */
      Role.get(id)
        .then((role) => {
          if (!role) return res.status(404).json(new NotFoundAPIResponse(req, this.modelName.toLowerCase()));
          req.role = role; // eslint-disable-line no-param-reassign
          return next();
        })
        .catch(() => res.status(404).json(new NotFoundAPIResponse(req, this.modelName.toLowerCase())));
    };
    /**
     * Get Role
     * @returns {Role}
     */
    this.get = (req, res) => {
      res.json(new OkDataResponse(req, `Role with id ${req.role.id}`, req.role));
    };

    /**
     * Create new Role
     * @property {string} req.body.name - The name of Role.
     * @returns {Role}
     */
    this.create = (req, res, next) => {
      const role = new Role({
        name: req.body.name,
      });
      /* istanbul ignore next */
      role.save()
        .then(c => res.json(new OkDataResponse(req, 'Role cretaed with success.', c)))
        .catch(e => next(e));
    };

    /**
     * Update existing Role
     * @property {string} req.body.name - The secret of Role.
     * @returns {Role}
     */
    this.update = (req, res, next) => {
      const role = req.role;
      role.name = req.body.name;
      /* istanbul ignore next */
      role.save()
        .then(c => res.json(new OkDataResponse(req, 'Role updated with success.', c)))
        .catch(e => next(e));
    };

    /**
     * Delete Role.
     * @returns {Role}
     */
    this.remove = (req, res, next) => {
      const role = req.role;
      /* istanbul ignore next */
      role.remove()
        .then(c => res.json(new OkDataResponse(req, 'Role deleted with success.', c)))
        .catch(e => next(e));
    };
  }
}
