import Contract from '../models/contract.model';
import BaseController from '../common/base.controller';
import { OkDataResponse, NotFoundAPIResponse } from '../helpers/APIResponse';

export default class ContractController extends BaseController {
  constructor(model) {
    super(model);

    /**
     * Load Contract and append to req.
     */
    this.load = (req, res, next, id) => {
      /* istanbul ignore next */
      Contract.get(id)
        .then((contract) => {
          if (!contract) return res.status(404).json(new NotFoundAPIResponse(req, this.modelName.toLowerCase()));
          req.contract = contract; // eslint-disable-line no-param-reassign
          return next();
        })
        .catch(() => res.status(404).json(new NotFoundAPIResponse(req, this.modelName.toLowerCase())));
    };
    /**
     * Get Contract
     * @returns {Contract}
     */
    this.get = (req, res) => {
      res.json(new OkDataResponse(req, `Contract with id ${req.contract.id}`, req.contract));
    };

    /**
     * Create new Contract
     * @property {string} req.body.name - The name of Contract.
     * @returns {Contract}
     */
    this.create = (req, res, next) => {
      const contract = new Contract({
        name: req.body.name,
      });
      /* istanbul ignore next */
      contract.save()
        .then(p => res.json(new OkDataResponse(req, 'Contract cretaed with success.', p)))
        .catch(e => next(e));
    };

    /**
     * Update existing Contract
     * @property {string} req.body.name - The secret of Contract.
     * @returns {Contract}
     */
    this.update = (req, res, next) => {
      const contract = req.contract;
      contract.name = req.body.name;
      /* istanbul ignore next */
      contract.save()
        .then(p => res.json(new OkDataResponse(req, 'Contract updated with success.', p)))
        .catch(e => next(e));
    };

    /**
     * Delete Contract.
     * @returns {Contract}
     */
    this.remove = (req, res, next) => {
      const contract = req.Contract;
      /* istanbul ignore next */
      contract.remove()
        .then(p => res.json(new OkDataResponse(req, 'Contract deleted with success.', p)))
        .catch(e => next(e));
    };
  }
}
