import Transaction from '../models/transaction.model';
import BaseController from '../common/base.controller';
import { OkDataResponse, NotFoundAPIResponse } from '../helpers/APIResponse';

export default class TransactionController extends BaseController {
  constructor(model) {
    super(model);

    /**
     * Load Transaction and append to req.
     */
    this.load = (req, res, next, id) => {
      /* istanbul ignore next */
      Transaction.get(id)
        .then((s) => {
          if (!s) return res.status(404).json(new NotFoundAPIResponse(req, this.modelName.toLowerCase()));
          req.transaction = s; // eslint-disable-line no-param-reassign
          return next();
        })
        .catch(() => res.status(404).json(new NotFoundAPIResponse(req, this.modelName.toLowerCase())));
    };
    /**
     * Get Transaction
     * @returns {Transaction}
     */
    this.get = (req, res) => {
      res.json(new OkDataResponse(req, `Transaction with id ${req.transaction.id}`, req.transaction));
    };

    /**
     * Create new Transaction
     * @property {string} req.body.name - The name of Transaction.
     * @returns {Transaction}
     */
    this.create = (req, res, next) => {
      const transaction = new Transaction({
        name: req.body.name,
      });
      /* istanbul ignore next */
      transaction.save()
        .then(s => res.json(new OkDataResponse(req, 'Transaction cretaed with success.', s)))
        .catch(e => next(e));
    };

    /**
     * Update existing Transaction
     * @property {string} req.body.name - The secret of Transaction.
     * @returns {Transaction}
     */
    this.update = (req, res, next) => {
      const transaction = req.transaction;
      /* istanbul ignore next */
      transaction.save()
        .then(s => res.json(new OkDataResponse(req, 'Transaction updated with success.', s)))
        .catch(e => next(e));
    };

    /**
     * Delete Transaction.
     * @returns {Transaction}
     */
    this.remove = (req, res, next) => {
      const transaction = req.transaction;
      /* istanbul ignore next */
      transaction.remove()
        .then(s => res.json(new OkDataResponse(req, 'Transaction deleted with success.', s)))
        .catch(e => next(e));
    };
  }
}
