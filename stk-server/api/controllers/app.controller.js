import config from '../../config/config';
import { OkDataResponse } from '../helpers/APIResponse';

export default class APPController {
  versioning = (req, res) => {
    const versionApp = {
      appVersion: config.versionAPP,
      apiVersion: config.versionApi,
    };
    return res.json(new OkDataResponse(req, 'SmarTokenizer Versioning', versionApp));
  };
}
