import jwt from 'jsonwebtoken';
import fs from 'fs';
import config from '../../config/config';

const response = {
  access_token: '',
  expires_in: 1209600,
  token_type: 'jwt',
  refresh_token: '',
  grant_type: 'access',
};

function createToken(payload) { // eslint-disable-line no-unused-vars
  const token = jwt.sign({
    data: (Math.random(1, 1000) * 25) % 6,
    exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 14),
  }, config.jwtSecret);

  const refreshToken = jwt.sign({
    data: (Math.random(1, 1000) * 25) % 6,
    exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 14),
  }, config.jwtSecret);

  response.access_token = token;
  response.refresh_token = refreshToken;
  return response;
}

function elementInArray(tmpElement, tmpArray) {
  let indexTmpArray = {};
  let elementExistsInArray = false;
  for (indexTmpArray = 0; indexTmpArray < tmpArray.length; indexTmpArray += 1) {
    if (tmpArray[indexTmpArray] === tmpElement) {
      elementExistsInArray = true;
      return elementExistsInArray;
    }
  }
  if (indexTmpArray >= tmpArray.length) { // sono uscito dal for senza risultato
    return elementExistsInArray;
  }
  return elementExistsInArray;
}

function createImage(picture, id) {
  const img = picture;
  const data = img.replace(/^data:image\/\w+;base64,/, '');
  const buf = new Buffer(data, 'base64');
  fs.writeFile(id.concat('.png'), buf);
}

function extractEmailFromArray(arr) {
  const email = [];
  arr.forEach((el) => {
    email.push(el.email);
  });
  return email;
}

function extractEmailFromUserArray(arr) {
  const email = [];
  arr.forEach((el) => {
    email.push(el.local.email);
  });
  return email;
}

export default {
  createToken,
  createImage,
  elementInArray,
  extractEmailFromArray,
  extractEmailFromUserArray,
};
