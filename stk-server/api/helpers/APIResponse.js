/* istanbul ignore else: base method */
import HTTPSTATUS from './HttpStatus';

class Response {
  constructor(code, status, meta, message) {
    this.metadata = {
      method: meta.method,
      code: code,
      status: status,
      host: meta.hostname,
      protocol: meta.protocol,
      url: meta.baseUrl + meta.path,
      param: meta.params,
      query: meta.query,
      message: message,
    };
  }
}

/**
 * Class representing an API error.
 * @extends Response
 */
class OkDataResponse extends Response {
  /**
  * Creates an API Response.if(!isNaN(data))
      this.count = data
    else
    this.data = data;
  * @param {number} message - custom ok message.
  * @param {boolean} data - the response data
  */
  constructor(meta, message, data) {
    super(HTTPSTATUS.OK.CODE, HTTPSTATUS.OK.STATUS, meta, message);
    if (data) {
      this.data = data;
    }
  }
}

/**
 * Class representing an API Rosponse for HTTP status 404.
 * @extends Response
 */
class NotFoundAPIResponse extends Response {
  /**
  * Creates an API Response.
  * @param {number} message - custom ok message.
  * @param {boolean} data - the response data
  */
  constructor(meta, model, customMessage) {
    super(HTTPSTATUS.NOT_FOUND.CODE, HTTPSTATUS.NOT_FOUND.STATUS, meta);
    if (model) {
      this.message = `No such ${model} with id = ${meta.params.id} exists!`;
    }
    /* istanbul ignore else: base method */
    if (customMessage) {
      this.message = customMessage;
    }
  }
}
/**
 * Class representing an API Rosponse for HTTP status 400.
 * @extends Response
 */
class BadRequestAPIResponse extends Response {
  /**
  * Creates an API Response.
  * @param {number} meta - metadata of request
  * @param {boolean} customMessage - the response custom message
  *//* istanbul ignore else: base method */
  constructor(meta, field, customMessage) {
    /* istanbul ignore if: base method */
    super(HTTPSTATUS.BAD_REQUEST.CODE, HTTPSTATUS.BAD_REQUEST.STATUS, meta);
    if (field) {
      this.message = `Missing request param! The field ${field} is mandatory.`;
    } else if (customMessage) {
      this.message = customMessage;
    } else {
      this.message = 'Invalid or missing request param!';
    }
  }
}

/**
 * Class representing an API Rosponse for HTTP status 403.
 * @extends Response
 */
class ForbiddenAPIResponse extends Response {
  /**
  * Creates an API Response.
  * @param {number} meta - metadata of request
  * @param {boolean} customMessage - the response custom message
  *//* istanbul ignore else: base method */
  constructor(meta, customMessage) {
    /* istanbul ignore if: base method */
    super(HTTPSTATUS.FORBIDDEN.CODE, HTTPSTATUS.FORBIDDEN.STATUS, meta);
    if (customMessage) {
      this.message = customMessage;
    } else {
      this.message = 'Invalid request param!';
    }
  }
}

/**
 * Class representing an API Rosponse for HTTP status 403.
 * @extends Response
 */
class ConflictAPIResponse extends Response {
  /**
  * Creates an API Response.
  * @param {number} meta - metadata of request
  * @param {boolean} customMessage - the response custom message
  *//* istanbul ignore else: base method */
  constructor(meta, customMessage) {
    /* istanbul ignore if: base method */
    super(HTTPSTATUS.CONFLICT.CODE, HTTPSTATUS.CONFLICT.STATUS, meta);
    if (customMessage) {
      this.message = customMessage;
    } else {
      this.message = 'Resource already present.Try with different param';
    }
  }
}

/**
 * Class representing an API Rosponse for HTTP status 5xx.
 * @extends Response
 */
class InternalServerErrorAPIResponse extends Response {
  /**
  * Creates an API Response.
  * @param {boolean} meta - the metadata of request
  *//* istanbul ignore else: base method */
  constructor(meta, customMessage) {
    /* istanbul ignore else: base method */
    super(HTTPSTATUS.INTERNAL_SERVER_ERROR.CODE, HTTPSTATUS.INTERNAL_SERVER_ERROR.STATUS, meta);
    if (customMessage) {
      this.message = customMessage;
    }
  }
}

/**
 * Class representing an API Rosponse for HTTP status 401.
 * @extends Response
 */
class UnauthorizeAPIResponse extends Response {
  /**
  * Creates an API Response.
  * @param {boolean} meta - the metadata of request
  *//* istanbul ignore else: base method */
  constructor(meta, customMessage) {
    /* istanbul ignore else: base method */
    super(HTTPSTATUS.UNAUTHORIZED.CODE, HTTPSTATUS.UNAUTHORIZED.STATUS, meta);
    if (customMessage) {
      this.message = customMessage;
    }
  }
}

/**
 * Class representing an API Rosponse for HTTP status 405.
 * @extends APIResponse
 */
class MethodNotAllowedAPIResponse extends Response {
  /**
  * Creates an API Response.
  * @param {boolean} meta - the metadata of request
  *//* istanbul ignore else: base method */
  constructor(meta, customMessage) {
    /* istanbul ignore else: base method */
    super(HTTPSTATUS.METHOD_NOT_ALLOWED.CODE, HTTPSTATUS.METHOD_NOT_ALLOWED.STATUS, meta);
    if (customMessage) {
      this.message = customMessage;
    }
  }
}

export default {
  OkDataResponse,
  NotFoundAPIResponse,
  BadRequestAPIResponse,
  ForbiddenAPIResponse,
  ConflictAPIResponse,
  UnauthorizeAPIResponse,
  MethodNotAllowedAPIResponse,
  InternalServerErrorAPIResponse,
};
