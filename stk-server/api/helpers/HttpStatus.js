/**
 * Constants enumerating the HTTP status CODEs.
 */

const HTTPSTATUS = {
  ACCEPTED: {
    CODE: 202,
    STATUS: 'Accepted',
  },
  BAD_GATEWAY: {
    CODE: 502,
    STATUS: 'Bad Gateway',
  },
  BAD_REQUEST: {
    CODE: 400,
    STATUS: 'Bad Request',
  },
  CONFLICT: {
    CODE: 409,
    STATUS: 'Conflict',
  },
  CONTINUE: {
    CODE: 100,
    STATUS: 'Continue',
  },
  CREATED: {
    CODE: 201,
    STATUS: 'Created',
  },
  EXPECTATION_FAILED: {
    CODE: 417,
    STATUS: 'Expectation Failed',
  },
  FAILED_DEPENDENCY: {
    CODE: 424,
    STATUS: 'Failed Dependency',
  },
  FORBIDDEN: {
    CODE: 403,
    STATUS: 'Forbidden',
  },
  GATEWAY_TIMEOUT: {
    CODE: 504,
    STATUS: 'Gateway Timeout',
  },
  GONE: {
    CODE: 410,
    STATUS: 'Gone',
  },
  HTTP_VERSION_NOT_SUPPORTED: {
    CODE: 505,
    STATUS: 'HTTP Version Not Supported',
  },
  INSUFFICIENT_SPACE_ON_RESOURCE: {
    CODE: 419,
    STATUS: 'Insufficient Space on Resource',
  },
  INSUFFICIENT_STORAGE: {
    CODE: 507,
    STATUS: 'Insufficient Storage',
  },
  INTERNAL_SERVER_ERROR: {
    CODE: 500,
    STATUS: 'Internal Server Error',
  },
  LENGTH_REQUIRED: {
    CODE: 411,
    STATUS: 'Length Required',
  },
  LOCKED: {
    CODE: 423,
    STATUS: 'Locked',
  },
  METHOD_FAILURE: {
    CODE: 420,
    STATUS: 'Method Failure',
  },
  METHOD_NOT_ALLOWED: {
    CODE: 405,
    STATUS: 'Method Not Allowed',
  },
  MOVED_PERMANENTLY: {
    CODE: 301,
    STATUS: 'Moved Permanently',
  },
  MOVED_TEMPORARILY: {
    CODE: 505,
    STATUS: 'Moved Temporarily',
  },
  MULTI_STATUS: {
    CODE: 207,
    STATUS: 'Multi-Status',
  },
  MULTIPLE_CHOICES: {
    CODE: 300,
    STATUS: 'Multiple Choices',
  },
  NETWORK_AUTHENTICATION_REQUIRED: {
    CODE: 511,
    STATUS: 'Network Authentication Required',
  },
  NO_CONTENT: {
    CODE: 204,
    STATUS: 'No Content',
  },
  NON_AUTHORITATIVE_INFORMATION: {
    CODE: 203,
    STATUS: 'Non Authoritative Information',
  },
  NOT_ACCEPTABLE: {
    CODE: 406,
    STATUS: 'Not Acceptable',
  },
  NOT_FOUND: {
    CODE: 404,
    STATUS: 'Not Found',
  },
  NOT_IMPLEMENTED: {
    CODE: 501,
    STATUS: 'Not Implemented',
  },
  NOT_MODIFIED: {
    CODE: 304,
    STATUS: 'Not Modified',
  },
  OK: {
    CODE: 200,
    STATUS: 'OK',
  },
  PARTIAL_CONTENT: {
    CODE: 406,
    STATUS: 'Partial Content',
  },
  PAYMENT_REQUIRED: {
    CODE: 402,
    STATUS: 'Payment Required',
  },
  PERMANENT_REDIRECT: {
    CODE: 308,
    STATUS: 'Permanent Redirect',
  },
  PRECONDITION_FAILED: {
    CODE: 412,
    STATUS: 'Precondition Failed',
  },
  PRECONDITION_REQUIRED: {
    CODE: 428,
    STATUS: 'Precondition Required',
  },
  PROCESSING: {
    CODE: 102,
    STATUS: 'Processing',
  },
  PROXY_AUTHENTICATION_REQUIRED: {
    CODE: 407,
    STATUS: 'Proxy Authentication Required',
  },
  REQUEST_HEADER_FIELDS_TOO_LARGE: {
    CODE: 431,
    STATUS: 'Request Header Fields Too Large',
  },
  REQUEST_TIMEOUT: {
    CODE: 408,
    STATUS: 'Request Timeout',
  },
  REQUEST_TOO_LONG: {
    CODE: 413,
    STATUS: 'Request Entity Too Large',
  },
  REQUEST_URI_TOO_LONG: {
    CODE: 414,
    STATUS: 'Request-URI Too Long',
  },
  REQUESTED_RANGE_NOT_SATISFIABLE: {
    CODE: 416,
    STATUS: 'Requested Range Not Satisfiable',
  },
  RESET_CONTENT: {
    CODE: 205,
    STATUS: 'Reset Content',
  },
  SEE_OTHER: {
    CODE: 303,
    STATUS: 'See Other',
  },
  SERVICE_UNAVAILABLE: {
    CODE: 503,
    STATUS: 'Service Unavailable',
  },
  SWITCHING_PROTOCOLS: {
    CODE: 101,
    STATUS: 'Switching Protocols',
  },
  TEMPORARY_REDIRECT: {
    CODE: 307,
    STATUS: 'Temporary Redirect',
  },
  TOO_MANY_REQUESTS: {
    CODE: 429,
    STATUS: 'Too Many Requests',
  },
  UNAUTHORIZED: {
    CODE: 401,
    STATUS: 'Unauthorized',
  },
  UNPROCESSABLE_ENTITY: {
    CODE: 422,
    STATUS: 'Unprocessable Entity',
  },
  UNSUPPORTED_MEDIA_TYPE: {
    CODE: 415,
    STATUS: 'Unsupported Media Type',
  },
  USE_PROXY: {
    CODE: 305,
    STATUS: 'Use Proxy',
  },
};

export default HTTPSTATUS;
