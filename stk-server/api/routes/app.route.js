import express from 'express';
import APPController from '../controllers/app.controller';
import MethodMiddleware from '../../config/method-middleware';

const router = express.Router();

const appCtrl = new APPController();

router.route('/version').all(MethodMiddleware.allowMethods(['get'])).get(appCtrl.versioning);
export default router;

