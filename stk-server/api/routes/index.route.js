import express from 'express';
import appRoutes from './app.route';
import roleRoutes from './role.route';
import userRoutes from './user.route';
import oauthRoutes from './auth.route';
import walletRoutes from './wallet.route';
import sessionRoutes from './session.route';
import projectRoutes from './project.route';
import contractRoutes from './contract.route';
import transactionRoutes from './transaction.route';
import config from '../../config/config';
import AuthenticatedMiddleware from '../../config/auth-middleware';
import MethodMiddleware from '../../config/method-middleware';

const router = express.Router({ automatic405: true }); // eslint-disable-line new-cap

/** GET /health-check - Check service health */
router.get('/', (req, res) => {
  res.send(`SmarTokenizer Server NodeJS running on port ${config.port}`);
});

/** mount oauth not protected routes at /api/v1/passes and allow on post methd **/
router.use('/auth', MethodMiddleware.allowMethods(['post', 'head']), oauthRoutes);

/** mount job routes at /api/v1/app **/
router.use('/app', appRoutes);

/** mount role routes at /api/v1/role **/
router.use('/role', AuthenticatedMiddleware, roleRoutes);

/** mount user routes at /api/v1/user **/
router.use('/user', AuthenticatedMiddleware, userRoutes);

/** mount user routes at /api/v1/contract **/
router.use('/contract', AuthenticatedMiddleware, contractRoutes);

/** mount user routes at /api/v1/projects **/
router.use('/projects', AuthenticatedMiddleware, projectRoutes);

/** mount session routes at /api/v1/wallet **/
router.use('/wallet', AuthenticatedMiddleware, walletRoutes);

/** mount session routes at /api/v1/session **/
router.use('/session', AuthenticatedMiddleware, sessionRoutes);

/** mount session routes at /api/v1/transaction **/
router.use('/transaction', AuthenticatedMiddleware, transactionRoutes);

export default router;
