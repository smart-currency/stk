import express from 'express';
import User from '../models/user.model';
import UserController from '../controllers/user.controller';
import MethodMiddleware from '../../config/method-middleware';

const router = express.Router(); // eslint-disable-line new-cap

const userCtrl = new UserController(User);
/**
 * @api {get} api/v1/users/ Get list of users
 * @apiName ListUser
 * @apiGroup User API
 *
 * @apiParam {String} username The username or email
 * @apiParam {String} password The user password
 *
 * @apiSuccess {User} user The user created
 */
router.route('/').get(userCtrl.findAll);
router.route('/count').all(MethodMiddleware.allowMethods(['get'])).get(userCtrl.count);
router.param('id', userCtrl.load);

export default router;
