import express from 'express';
import Wallet from '../models/wallet.model';
import WalletController from '../controllers/wallet.controller';
import MethodMiddleware from '../../config/method-middleware';


const router = express.Router();

const walletCtrl = new WalletController(Wallet);

router.route('/count').all(MethodMiddleware.allowMethods(['get'])).get(walletCtrl.count);
router.route('/').get(walletCtrl.list);
router.route('/').post(walletCtrl.create);
router.route('/:id').get(walletCtrl.get);
router.route('/:id').put(walletCtrl.update);
router.route('/:id').delete(walletCtrl.remove);
router.param('id', walletCtrl.load);
export default router;
