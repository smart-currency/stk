import express from 'express';
import Session from '../models/session.model';
import SessionController from '../controllers/session.controller';
import MethodMiddleware from '../../config/method-middleware';

const router = express.Router();

const sessionCtrl = new SessionController(Session);

router.route('/count').all(MethodMiddleware.allowMethods(['get'])).get(sessionCtrl.count);
router.route('/').get(sessionCtrl.list);
router.route('/').post(sessionCtrl.create);
router.route('/:id').get(sessionCtrl.get);
router.route('/:id').put(sessionCtrl.update);
router.route('/:id').delete(sessionCtrl.remove);
router.param('id', sessionCtrl.load);
export default router;

