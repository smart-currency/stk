import express from 'express';
import Role from '../models/role.model';
import RoleController from '../controllers/role.controller';
import MethodMiddleware from '../../config/method-middleware';

const router = express.Router();

const roleCtrl = new RoleController(Role);

router.route('/count').all(MethodMiddleware.allowMethods(['get'])).get(roleCtrl.count);
router.route('/').get(roleCtrl.list);
router.route('/').post(roleCtrl.create);
router.route('/:id').get(roleCtrl.get);
router.route('/:id').put(roleCtrl.update);
router.route('/:id').delete(roleCtrl.remove);
router.param('id', roleCtrl.load);
export default router;

