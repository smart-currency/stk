import express from 'express';
import Contract from '../models/contract.model';
import ContractController from '../controllers/contract.controller';
import MethodMiddleware from '../../config/method-middleware';

const router = express.Router();

const contractCtrl = new ContractController(Contract);

router.route('/count').all(MethodMiddleware.allowMethods(['get'])).get(contractCtrl.count);
router.route('/').get(contractCtrl.list);
router.route('/').post(contractCtrl.create);
router.route('/:id').get(contractCtrl.get);
router.route('/:id').put(contractCtrl.update);
router.route('/:id').delete(contractCtrl.remove);
router.param('id', contractCtrl.load);
export default router;

