import express from 'express';
import Transaction from '../models/transaction.model';
import TransactionController from '../controllers/role.controller';
import MethodMiddleware from '../../config/method-middleware';

const router = express.Router();

const transactionCtrl = new TransactionController(Transaction);

router.route('/count').all(MethodMiddleware.allowMethods(['get'])).get(transactionCtrl.count);
router.route('/').get(transactionCtrl.list);
router.route('/').post(transactionCtrl.create);
router.route('/:id').get(transactionCtrl.get);
router.route('/:id').put(transactionCtrl.update);
router.route('/:id').delete(transactionCtrl.remove);
router.param('id', transactionCtrl.load);
export default router;

