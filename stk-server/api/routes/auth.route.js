import express from 'express';
import AuthController from '../controllers/auth.controller';
import MethodMiddleware from '../../config/method-middleware';

const router = express.Router();

const authCtrl = new AuthController();

router.route('/token').all(MethodMiddleware.allowMethods(['post'])).post(authCtrl.createToken);
router.route('/authorize').all(MethodMiddleware.allowMethods(['post'])).post(authCtrl.createToken);

export default router;

