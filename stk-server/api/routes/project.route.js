import express from 'express';
import Project from '../models/project.model';
import ProjectController from '../controllers/project.controller';
import MethodMiddleware from '../../config/method-middleware';

const router = express.Router();

const projectCtrl = new ProjectController(Project);

router.route('/count').all(MethodMiddleware.allowMethods(['get'])).get(projectCtrl.count);
router.route('/').get(projectCtrl.list);
router.route('/').post(projectCtrl.create);
router.route('/:id').get(projectCtrl.get);
router.route('/:id').put(projectCtrl.update);
router.route('/:id').delete(projectCtrl.remove);
router.param('id', projectCtrl.load);
export default router;

