import mongoose from 'mongoose';

/**
 *  Base Model
 */
class BaseSchema extends mongoose.Schema {
  constructor(definition) {
    super(definition);
    this.options.versionKey = false;

    this.statics = {
      get(id) {
        return this.findById(id)
          .exec();
      },
      list(limit) {
        return this.find({})
          .limit(+limit)
          .exec();
      },
      listWithPopulator(limit, populator) {
        return this.find({})
        .populate(populator)
        .limit(+limit)
        .exec();
      },
      findBy(whereClausule, value) {
        return this.find({})
          .where(whereClausule).equals(value)
          .exec();
      },
      toObjectId(id) {
        return mongoose.Types.ObjectId(id);
      },
    };
    this.set('toJSON', {
      virtuals: true,
      transform: (doc, ret) => {
        delete ret.__v; // eslint-disable-line no-param-reassign
        ret.id = ret._id.toString(); // eslint-disable-line no-param-reassign
        if (ret.user_id) { // eslint-disable-line no-param-reassign
          ret.userId = ret.user_id; // eslint-disable-line no-param-reassign
          delete ret.user_id; // eslint-disable-line no-param-reassign
        }
        if (ret.redirect_uri) {
          ret.redirectUri = ret.redirect_uri; // eslint-disable-line no-param-reassign
          delete ret.redirect_uri; // eslint-disable-line no-param-reassign
        }
        delete ret._id; // eslint-disable-line no-param-reassign
      },
    });
  }
}
export default BaseSchema;
