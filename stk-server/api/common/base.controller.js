import { OkDataResponse } from '../helpers/APIResponse';

/**
  Generic controller that provides CRUD operations for a given Mongoose model
*/
export default class BaseController {

  constructor(model) {
    this.model = model; /** pass the model object **/
    this.modelName = model.modelName;

    /* istanbul ignore next */
    this.list = (req, res, next) => {
      const {
        limit = (req.query.limit) ? req.query.limit : 15,
      } = req.query;
      this.model.list(limit)
        .then(models => res.json(new OkDataResponse(req, `List of ${this.modelName.toLowerCase()}`, models)))
        .catch(e => next(e));
    };

    /* istanbul ignore next */
    this.count = (req, res, next) => {
      this.model.count().then((c) => {
        if (!c) return res.json(new OkDataResponse(req, `No ${this.modelName.toLowerCase()} found!`));
        return res.json(new OkDataResponse(req, `The number of ${this.modelName.toLowerCase()} present into the database`, c));
      })
        .catch(e => next(e));
    };
  }
}
