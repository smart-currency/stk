const Constants = {
  EMPTY_STRING: '',
  EMPTY_NUMER: '4598673548',
  SAMRT_TOKENIZER: 'SAMRT_TOKENIZER',
  EMAIL_DEFAULT: 'info@smartokenizer.com',
  STATE_DEFAULT: 'Italia',
  LANGUAGE_DEFAULT: 'Italiano',
  LOGO_URL_DEFAULT: '',
  AVATAR_URL_DEFAUL: '',
  METHOD: {
    BCRYPT_SALT_VALUE: 10,
  },
  MODEL: {
    USER: 'User',
    ROLE: 'Role',
    WALLET: 'Wallet',
    PROJECT: 'Project',
    CONTRACT: 'Contract',
    SESSION: 'Session',
    TRANSACTION: 'Transaction',
  },
  WEBSITE: {
    DISABLE: 'non disponibile',
  },
  ENUM: {
    SUCCESS: 'success',
    FAILURE: 'failure',
    PENDING: 'pending',
  },
  ROLES: {
    USER: 'USER',
    TEAM: 'TEAM',
  },
  MESSAGES: {
  },
  VALUE: {
    DOUBLE: 0,
    STRING: '',
    BOOLEAN: false,
    NULL: null,
    URL_IMAGE: '',
    AVATAR_DEFAULT: 'https://www.gravatar.com/avatar/34r093eda667addd6e650d7e9881bcad?s=32&d=identicon&r=PG',
  },
  ICON_DEFAULT: 'https://www.gravatar.com/avatar/34r093eda667addd6e650d7e9881bcad?s=32&d=identicon&r=PG',
};

export default Constants;
