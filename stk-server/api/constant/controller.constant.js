/* istanbul ignore */
const Constants = {
  PROVIDER: {
    FACEBOOK: 'facebook',
    TWITTER: 'twitter',
    GOOGLE: 'google',
  },
  HTTPMESSAGE: {
    GET_USERS: 'list of user',
    USER_COUNT: 'the number of user',
    NOT_FOUND: 'not found',
  },
};

export default Constants;
